﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Sessions
{
    public partial class SessionEditorForm : Form
    {
        public Session session;

        public SessionEditorForm(Session session)
        {
            InitializeComponent();
            this.session = session;
            sessionTypeComboBox.DataSource = SessionTypesData.GetSessionTypes();
            sessionTypeComboBox.DisplayMember = "label";
            sessionTypeComboBox.SelectedItem = session.SessionType;
            startTimePicker.Format = DateTimePickerFormat.Custom;
            startTimePicker.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            endTimePicker.Format = DateTimePickerFormat.Custom;
            endTimePicker.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            startTimePicker.Value = session.startTime == DateTime.MinValue ? DateTime.Now : session.startTime;
            endTimePicker.Value = session.endTime == DateTime.MinValue ? DateTime.Now.AddHours(1) : session.endTime;
        }

        public SessionEditorForm(Session session, List<SessionType> sessionType)
        {
            InitializeComponent();
            this.session = session;
            sessionTypeComboBox.DataSource = sessionType;
            sessionTypeComboBox.DisplayMember = "label";
            sessionTypeComboBox.SelectedItem = session.SessionType;
            startTimePicker.Format = DateTimePickerFormat.Custom;
            startTimePicker.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            endTimePicker.Format = DateTimePickerFormat.Custom;
            endTimePicker.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            startTimePicker.Value = session.startTime == DateTime.MinValue ? DateTime.Now : session.startTime;
            endTimePicker.Value = session.endTime == DateTime.MinValue ? DateTime.Now.AddHours(1) : session.endTime;
            session.startTime = session.startTime == DateTime.MinValue ? DateTime.Now : session.startTime;
            session.endTime = session.endTime == DateTime.MinValue ? DateTime.Now.AddHours(1) : session.endTime;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (((SessionType)sessionTypeComboBox.SelectedItem) == null)
            {
                MessageBox.Show("Fill in all the fields");
                return;
            }
            if (session.startTime.CompareTo(session.endTime) >= 0)
            {
                MessageBox.Show("Period is inappropriate!");
                return;
            }
            session.SessionType = (SessionType)sessionTypeComboBox.SelectedItem;
            ErrorCatcher error = SessionsData.SaveSession(session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void startTimePicker_ValueChanged(object sender, EventArgs e)
        {
            session.startTime = startTimePicker.Value;
        }

        private void endTimePicker_ValueChanged(object sender, EventArgs e)
        {
            session.endTime = endTimePicker.Value;
        }
    }
}
