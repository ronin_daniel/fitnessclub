﻿
namespace FitnessClub.Forms.Sessions
{
    partial class SessionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sessionsTable = new System.Windows.Forms.DataGridView();
            this.sessionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.fitnessClubDataSet = new FitnessClub.FitnessClubDataSet();
            this.sessionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.addCoachButton = new System.Windows.Forms.Button();
            this.arrangeCoachButton = new System.Windows.Forms.Button();
            this.deleteCoach = new System.Windows.Forms.Button();
            this.fitnessClubDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sessionTableAdapter = new FitnessClub.FitnessClubDataSetTableAdapters.SessionTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.sessionsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fitnessClubDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fitnessClubDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // sessionsTable
            // 
            this.sessionsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sessionsTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.sessionsTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sessionsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sessionsTable.Location = new System.Drawing.Point(12, 12);
            this.sessionsTable.MultiSelect = false;
            this.sessionsTable.Name = "sessionsTable";
            this.sessionsTable.ReadOnly = true;
            this.sessionsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sessionsTable.Size = new System.Drawing.Size(776, 331);
            this.sessionsTable.TabIndex = 0;
            // 
            // sessionBindingSource1
            // 
            this.sessionBindingSource1.DataMember = "Session";
            this.sessionBindingSource1.DataSource = this.fitnessClubDataSet;
            // 
            // fitnessClubDataSet
            // 
            this.fitnessClubDataSet.DataSetName = "FitnessClubDataSet";
            this.fitnessClubDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // editButton
            // 
            this.editButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editButton.BackColor = System.Drawing.Color.Chocolate;
            this.editButton.Enabled = false;
            this.editButton.Location = new System.Drawing.Point(378, 363);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 75);
            this.editButton.TabIndex = 12;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = false;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteButton.BackColor = System.Drawing.Color.Chocolate;
            this.deleteButton.Location = new System.Drawing.Point(198, 363);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 75);
            this.deleteButton.TabIndex = 11;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // addCoachButton
            // 
            this.addCoachButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addCoachButton.BackColor = System.Drawing.Color.Chocolate;
            this.addCoachButton.Location = new System.Drawing.Point(13, 363);
            this.addCoachButton.Name = "addCoachButton";
            this.addCoachButton.Size = new System.Drawing.Size(75, 75);
            this.addCoachButton.TabIndex = 10;
            this.addCoachButton.Text = "Add";
            this.addCoachButton.UseVisualStyleBackColor = false;
            this.addCoachButton.Click += new System.EventHandler(this.addCoachButton_Click);
            // 
            // arrangeCoachButton
            // 
            this.arrangeCoachButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.arrangeCoachButton.BackColor = System.Drawing.Color.Chocolate;
            this.arrangeCoachButton.Location = new System.Drawing.Point(713, 404);
            this.arrangeCoachButton.Name = "arrangeCoachButton";
            this.arrangeCoachButton.Size = new System.Drawing.Size(75, 35);
            this.arrangeCoachButton.TabIndex = 13;
            this.arrangeCoachButton.Text = "Arrange coach";
            this.arrangeCoachButton.UseVisualStyleBackColor = false;
            this.arrangeCoachButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // deleteCoach
            // 
            this.deleteCoach.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteCoach.BackColor = System.Drawing.Color.Chocolate;
            this.deleteCoach.Location = new System.Drawing.Point(713, 363);
            this.deleteCoach.Name = "deleteCoach";
            this.deleteCoach.Size = new System.Drawing.Size(75, 35);
            this.deleteCoach.TabIndex = 14;
            this.deleteCoach.Text = "Detach coach";
            this.deleteCoach.UseVisualStyleBackColor = false;
            this.deleteCoach.Click += new System.EventHandler(this.deleteCoach_Click);
            // 
            // fitnessClubDataSetBindingSource
            // 
            this.fitnessClubDataSetBindingSource.DataSource = this.fitnessClubDataSet;
            this.fitnessClubDataSetBindingSource.Position = 0;
            // 
            // sessionTableAdapter
            // 
            this.sessionTableAdapter.ClearBeforeFill = true;
            // 
            // SessionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.deleteCoach);
            this.Controls.Add(this.arrangeCoachButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addCoachButton);
            this.Controls.Add(this.sessionsTable);
            this.Name = "SessionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sessions";
            this.Load += new System.EventHandler(this.SessionsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sessionsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fitnessClubDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fitnessClubDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView sessionsTable;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button addCoachButton;
        private System.Windows.Forms.Button arrangeCoachButton;
        private System.Windows.Forms.Button deleteCoach;
        private System.Windows.Forms.BindingSource sessionBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource fitnessClubDataSetBindingSource;
        private FitnessClubDataSet fitnessClubDataSet;
        private System.Windows.Forms.BindingSource sessionBindingSource1;
        private FitnessClubDataSetTableAdapters.SessionTableAdapter sessionTableAdapter;
    }
}