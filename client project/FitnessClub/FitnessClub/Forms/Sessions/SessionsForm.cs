﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Sessions
{
    public partial class SessionsForm : Form
    {
        public SessionsForm()
        {
            InitializeComponent();
            fetchSessions();
        }

        private void fetchSessions()
        {
            SessionViewErrorCatcher error = SessionsData.GetSessionViews();
            if (error.isError())
            {
                MessageBox.Show(error.message);
                return;
            }
            sessionsTable.DataSource = error.sessions;
            sessionsTable.Columns["idSession"].Visible = false;
            sessionsTable.Columns["idSessionType"].Visible = false;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a session");
                return;
            }
            SessionView sessionView = (SessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }
            Session session = sessionError.session;
            ErrorCatcher error = SessionsData.DeleteSession(session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchSessions();
        }

        private void addCoachButton_Click(object sender, EventArgs e)
        {
            SessionEditorForm dialog = new SessionEditorForm(new Session());
            dialog.ShowDialog();
            fetchSessions();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a session");
                return;
            }
            SessionView sessionView = (SessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }
            Session session = sessionError.session;
            SessionEditorForm dialog = new SessionEditorForm(session);
            dialog.ShowDialog();
            fetchSessions();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a session");
                return;
            }
            SessionView sessionView = (SessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }
            Session session = sessionError.session;
            CoachesDialog dialog = new CoachesDialog(session, false);
            dialog.ShowDialog();
        }

        private void deleteCoach_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a session");
                return;
            }
            SessionView sessionView = (SessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }
            Session session = sessionError.session;
            CoachesDialog dialog = new CoachesDialog(session, true);
            dialog.ShowDialog();
        }

        private void SessionsForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'fitnessClubDataSet.Session' table. You can move, or remove it, as needed.
            this.sessionTableAdapter.Fill(this.fitnessClubDataSet.Session);

        }
    }
}
