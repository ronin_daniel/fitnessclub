﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Sessions
{
    public partial class CoachesDialog : Form
    {
        Session session;
        bool isDelete;

        public CoachesDialog(Session session)
        {
            InitializeComponent();
            this.session = session;
            coachesTable.DataSource = CoachesData.GetCoaches();
            coachesTable.Columns["idCoach"].Visible = false;
            coachesTable.Columns["password"].Visible = false;
            coachesTable.Columns["Sessions"].Visible = false;
            coachesTable.Columns["SessionTypes"].Visible = false;
        }

        public CoachesDialog(Session session, SessionType sessionType)
        {
            InitializeComponent();
            this.session = session;
            coachesTable.DataSource = CoachesData.GetCoachesBySessionType(sessionType);
            coachesTable.Columns["idCoach"].Visible = false;
            coachesTable.Columns["password"].Visible = false;
            coachesTable.Columns["Sessions"].Visible = false;
            coachesTable.Columns["SessionTypes"].Visible = false;
        }

        public CoachesDialog(Session session, bool isDelete)
        {
            InitializeComponent();
            this.session = session;
            this.isDelete = isDelete;
            if (isDelete)
            {
                coachesTable.DataSource = CoachesData.GetCoachesBySession(session);
            }
            else
            {
                coachesTable.DataSource = CoachesData.GetCoachesByNotInSession(session);
            }
            coachesTable.Columns["idCoach"].Visible = false;
            coachesTable.Columns["password"].Visible = false;
            coachesTable.Columns["Sessions"].Visible = false;
            coachesTable.Columns["SessionTypes"].Visible = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (coachesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a coach");
                return;
            }

            if (isDelete)
            {
                DeattachCoach((Coach)coachesTable.SelectedRows[0].DataBoundItem);
            }
            else
            {
                ArrangeCoach((Coach)coachesTable.SelectedRows[0].DataBoundItem);
            }

            
        }

        private void DeattachCoach(Coach coach)
        {
            ErrorCatcher error = SessionsData.RearrangeCoach(session, coach);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.Close();
            }
        }

        private void ArrangeCoach(Coach coach)
        {
            ErrorCatcher error = SessionsData.ArrangeCoach(session, coach);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.Close();
            }
        }

        private void CoachesDialog_Load(object sender, EventArgs e)
        {

        }
    }
}
