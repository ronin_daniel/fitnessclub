﻿
namespace FitnessClub.Forms
{
    partial class CoachActionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.sessionsTable = new System.Windows.Forms.DataGridView();
            this.sessionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.addSessionButton = new System.Windows.Forms.Button();
            this.sessionTypesButton = new System.Windows.Forms.Button();
            this.deleteSessionButton = new System.Windows.Forms.Button();
            this.arrangeSessionButton = new System.Windows.Forms.Button();
            this.editSessionButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sessionsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(111, 9);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(74, 13);
            this.statusLabel.TabIndex = 3;
            this.statusLabel.Text = "Status: Coach";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(11, 9);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(41, 13);
            this.nameLabel.TabIndex = 2;
            this.nameLabel.Text = "Name: ";
            // 
            // sessionsTable
            // 
            this.sessionsTable.AllowUserToAddRows = false;
            this.sessionsTable.AllowUserToDeleteRows = false;
            this.sessionsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sessionsTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.sessionsTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sessionsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sessionsTable.Location = new System.Drawing.Point(14, 44);
            this.sessionsTable.MultiSelect = false;
            this.sessionsTable.Name = "sessionsTable";
            this.sessionsTable.ReadOnly = true;
            this.sessionsTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.sessionsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sessionsTable.Size = new System.Drawing.Size(393, 250);
            this.sessionsTable.TabIndex = 4;
            // 
            // addSessionButton
            // 
            this.addSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addSessionButton.BackColor = System.Drawing.Color.Chocolate;
            this.addSessionButton.Location = new System.Drawing.Point(429, 44);
            this.addSessionButton.Name = "addSessionButton";
            this.addSessionButton.Size = new System.Drawing.Size(120, 23);
            this.addSessionButton.TabIndex = 5;
            this.addSessionButton.Text = "Arrange coach";
            this.addSessionButton.UseVisualStyleBackColor = false;
            this.addSessionButton.Click += new System.EventHandler(this.addSessionButton_Click);
            // 
            // sessionTypesButton
            // 
            this.sessionTypesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionTypesButton.BackColor = System.Drawing.Color.Chocolate;
            this.sessionTypesButton.Location = new System.Drawing.Point(429, 238);
            this.sessionTypesButton.Name = "sessionTypesButton";
            this.sessionTypesButton.Size = new System.Drawing.Size(120, 23);
            this.sessionTypesButton.TabIndex = 6;
            this.sessionTypesButton.Text = "Set session type";
            this.sessionTypesButton.UseVisualStyleBackColor = false;
            this.sessionTypesButton.Click += new System.EventHandler(this.sessionTypesButton_Click);
            // 
            // deleteSessionButton
            // 
            this.deleteSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteSessionButton.BackColor = System.Drawing.Color.Chocolate;
            this.deleteSessionButton.Location = new System.Drawing.Point(429, 73);
            this.deleteSessionButton.Name = "deleteSessionButton";
            this.deleteSessionButton.Size = new System.Drawing.Size(120, 23);
            this.deleteSessionButton.TabIndex = 7;
            this.deleteSessionButton.Text = "Remove coach";
            this.deleteSessionButton.UseVisualStyleBackColor = false;
            this.deleteSessionButton.Click += new System.EventHandler(this.deleteSessionButton_Click);
            // 
            // arrangeSessionButton
            // 
            this.arrangeSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.arrangeSessionButton.BackColor = System.Drawing.Color.Chocolate;
            this.arrangeSessionButton.Location = new System.Drawing.Point(429, 130);
            this.arrangeSessionButton.Name = "arrangeSessionButton";
            this.arrangeSessionButton.Size = new System.Drawing.Size(120, 23);
            this.arrangeSessionButton.TabIndex = 8;
            this.arrangeSessionButton.Text = "Arrange session";
            this.arrangeSessionButton.UseVisualStyleBackColor = false;
            this.arrangeSessionButton.Click += new System.EventHandler(this.arrangeSessionButton_Click);
            // 
            // editSessionButton
            // 
            this.editSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editSessionButton.BackColor = System.Drawing.Color.Chocolate;
            this.editSessionButton.Enabled = false;
            this.editSessionButton.Location = new System.Drawing.Point(429, 159);
            this.editSessionButton.Name = "editSessionButton";
            this.editSessionButton.Size = new System.Drawing.Size(120, 23);
            this.editSessionButton.TabIndex = 9;
            this.editSessionButton.Text = "Edit session";
            this.editSessionButton.UseVisualStyleBackColor = false;
            this.editSessionButton.Click += new System.EventHandler(this.editSessionButton_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Chocolate;
            this.button1.Location = new System.Drawing.Point(429, 267);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Remove session type";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.DeleteSessionTypeButton_Click);
            // 
            // CoachActionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(584, 321);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.editSessionButton);
            this.Controls.Add(this.arrangeSessionButton);
            this.Controls.Add(this.deleteSessionButton);
            this.Controls.Add(this.sessionTypesButton);
            this.Controls.Add(this.addSessionButton);
            this.Controls.Add(this.sessionsTable);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.nameLabel);
            this.Name = "CoachActionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Coach";
            this.Load += new System.EventHandler(this.CoachActionsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sessionsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.DataGridView sessionsTable;
        private System.Windows.Forms.Button addSessionButton;
        private System.Windows.Forms.Button sessionTypesButton;
        private System.Windows.Forms.Button deleteSessionButton;
        private System.Windows.Forms.Button arrangeSessionButton;
        private System.Windows.Forms.Button editSessionButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource sessionBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endTimeDataGridViewTextBoxColumn;
    }
}