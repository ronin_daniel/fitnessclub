﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Forms.SessionTypes;
using FitnessClub.Forms.Coaches;
using FitnessClub.Forms.SusbcriptionTypes;
using FitnessClub.Forms.Clients;
using FitnessClub.Forms.Sessions;
using FitnessClub.Forms;

namespace FitnessClub
{
    public partial class AdminActionsForm : Form
    {
        public AdminActionsForm()
        {
            InitializeComponent();
            nameLabel.Text = "Name: " + CurrentUser.name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form form = new Coaches();
            form.ShowDialog();
        }

        private void sessionTypesButton_Click(object sender, EventArgs e)
        {
            Form form = new SessionTypesForm();
            form.ShowDialog();
        }

        private void subcriptionTypesButton_Click(object sender, EventArgs e)
        {
            Form form = new SubcriptionTypesForm();
            form.ShowDialog();
        }

        private void clientsButton_Click(object sender, EventArgs e)
        {
            Form form = new ClientsForm();
            form.ShowDialog();
        }

        private void sessionsButton_Click(object sender, EventArgs e)
        {
            Form form = new SessionsForm();
            form.ShowDialog();
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            Form form = new ConnectionStringForm();
            form.ShowDialog();
        }
    }
}
