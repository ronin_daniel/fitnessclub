﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;
using FitnessClub.Forms.Coaches;
using FitnessClub.Forms.Sessions;

namespace FitnessClub.Forms
{
    public partial class CoachActionsForm : Form
    {
        Coach coach;

        public CoachActionsForm()
        {
            InitializeComponent();
            nameLabel.Text = "Name: " + CurrentUser.name;
            CoachErrorCatcher error = CoachesData.GetCoachByCurrentUser();
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                coach = error.coach;
            }
            fetchSessions();
        }

        private void addSessionButton_Click(object sender, EventArgs e)
        {
            CoachSessionDialog dialog = new CoachSessionDialog(coach);
            dialog.ShowDialog();
            fetchSessions();
        }

        private void fetchSessions()
        {
            CoachSessionErrorCatcher sessionsError = SessionsData.GetCoachSessions(coach.idCoach);
            if (sessionsError.isError())
            {
                MessageBox.Show(sessionsError.message);
            }
            else
            {
                sessionsTable.DataSource = sessionsError.sessions;
                sessionsTable.Columns["idSession"].Visible = false;
                sessionsTable.Columns["idSessionType"].Visible = false;
                sessionsTable.Columns["idCoach"].Visible = false;
                //sessionsTable.Columns.Add("SessionTypeName", "SessionType");

                //foreach (DataGridViewRow row in sessionsTable.Rows)
                //{
                //    Session session = (Session)row.DataBoundItem;
                //    row.Cells["SessionTypeName"].ValueType = typeof(String);
                //    row.Cells["SessionTypeName"].Value = session.SessionType.label;
                //}
            }
        }
        private void deleteSession(Session session)
        {
            ErrorCatcher error = SessionsData.DeleteSession(session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
        }

        private void deleteSessionButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                return;
            }
            CoachSessionView coachSession = (CoachSessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(coachSession.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }

            Session session = sessionError.session;

            ErrorCatcher error = SessionsData.RearrangeCoach(session, coach);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchSessions();
        }

        private void arrangeSessionButton_Click(object sender, EventArgs e)
        {
            Session session = new Session();
            SessionEditorForm form = new SessionEditorForm(session, coach.SessionTypes.ToList());
            DialogResult result = form.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }
            ErrorCatcher error = SessionsData.ArrangeCoach(session, coach);
            if (error.isError())
            {
                MessageBox.Show(error.message);
                deleteSession(session);
            }
            fetchSessions();
        }

        private void editSessionButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                return;
            }

            CoachSessionView coachSession = (CoachSessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher error = SessionsData.GetSession(coachSession.idSession);
            if (error.isError())
            {
                MessageBox.Show(error.message);
                return;
            }

            Session session = error.session;
            SessionEditorForm form = new SessionEditorForm(session, coach.SessionTypes.ToList());
            form.ShowDialog();
            Core.Context.CoachSessionViews.Attach(coachSession);
            Core.Context.Entry(coachSession).Reload();
            fetchSessions();
        }

        private void sessionTypesButton_Click(object sender, EventArgs e)
        {
            SessionTypeDialog dialog = new SessionTypeDialog(coach, false);
            dialog.ShowDialog();
            fetchSessions();
        }

        private void CoachActionsForm_Load(object sender, EventArgs e)
        {

        }

        private void DeleteSessionTypeButton_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                "Are you sure you want to delete session type of this coach?" +
                " The sessions concerned will be left without coach!", 
                "Warning!",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                SessionTypeDialog dialog = new SessionTypeDialog(coach, true);
                dialog.ShowDialog();
                fetchSessions();
            }
        }
    }
}
