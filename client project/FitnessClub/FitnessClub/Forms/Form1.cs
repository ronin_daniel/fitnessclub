﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Forms;

namespace FitnessClub
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void adminButton_Click(object sender, EventArgs e)
        {
            Form form = new AdminActionsForm();
            form.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void signButton_Click(object sender, EventArgs e)
        {
            string name = nameTextBox.Text.Trim();
            string password = passwordTextBox.Text.Trim();
            User user = Core.Context.Users.Where(u => u.name == name && u.password == password).FirstOrDefault();
            if (user != null)
            {
                CurrentUser.name = name;
                CurrentUser.password = password;
                Form form;
                switch (user.userRole.Trim())
                {
                    case "admin":
                        CurrentUser.role = UserRole.admin;
                        form = new AdminActionsForm();
                        form.ShowDialog();
                        break;
                    case "client":
                        CurrentUser.role = UserRole.client;
                        form = new ClientActionsForm();
                        form.ShowDialog();
                        break;
                    case "coach":
                        CurrentUser.role = UserRole.coach;
                        form = new CoachActionsForm();
                        form.ShowDialog();
                        break;
                }
            }
            else
            {
                DialogResult result = MessageBox.Show("There is no user with such credentials. Sign up?", "Sign up", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    SignUpForm dialog = new SignUpForm();
                    dialog.ShowDialog();
                }
            }
        }

        private void signUpButton_Click(object sender, EventArgs e)
        {
            SignUpForm dialog = new SignUpForm();
            dialog.ShowDialog();
        }
    }
}
