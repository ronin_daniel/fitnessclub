﻿
namespace FitnessClub
{
    partial class AdminActionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.sessionTypesButton = new System.Windows.Forms.Button();
            this.subcriptionTypesButton = new System.Windows.Forms.Button();
            this.clientsButton = new System.Windows.Forms.Button();
            this.sessionsButton = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.settingsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.Chocolate;
            this.button1.Location = new System.Drawing.Point(28, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 100);
            this.button1.TabIndex = 0;
            this.button1.Text = "Coaches";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // sessionTypesButton
            // 
            this.sessionTypesButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sessionTypesButton.AutoSize = true;
            this.sessionTypesButton.BackColor = System.Drawing.Color.Chocolate;
            this.sessionTypesButton.Location = new System.Drawing.Point(134, 164);
            this.sessionTypesButton.Name = "sessionTypesButton";
            this.sessionTypesButton.Size = new System.Drawing.Size(100, 100);
            this.sessionTypesButton.TabIndex = 1;
            this.sessionTypesButton.Text = "SessionTypes";
            this.sessionTypesButton.UseVisualStyleBackColor = false;
            this.sessionTypesButton.Click += new System.EventHandler(this.sessionTypesButton_Click);
            // 
            // subcriptionTypesButton
            // 
            this.subcriptionTypesButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.subcriptionTypesButton.AutoSize = true;
            this.subcriptionTypesButton.BackColor = System.Drawing.Color.Chocolate;
            this.subcriptionTypesButton.Location = new System.Drawing.Point(28, 164);
            this.subcriptionTypesButton.Name = "subcriptionTypesButton";
            this.subcriptionTypesButton.Size = new System.Drawing.Size(100, 100);
            this.subcriptionTypesButton.TabIndex = 2;
            this.subcriptionTypesButton.Text = "Subcription types";
            this.subcriptionTypesButton.UseVisualStyleBackColor = false;
            this.subcriptionTypesButton.Click += new System.EventHandler(this.subcriptionTypesButton_Click);
            // 
            // clientsButton
            // 
            this.clientsButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.clientsButton.BackColor = System.Drawing.Color.Chocolate;
            this.clientsButton.Location = new System.Drawing.Point(28, 270);
            this.clientsButton.Name = "clientsButton";
            this.clientsButton.Size = new System.Drawing.Size(206, 31);
            this.clientsButton.TabIndex = 3;
            this.clientsButton.Text = "Clients";
            this.clientsButton.UseVisualStyleBackColor = false;
            this.clientsButton.Click += new System.EventHandler(this.clientsButton_Click);
            // 
            // sessionsButton
            // 
            this.sessionsButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sessionsButton.AutoSize = true;
            this.sessionsButton.BackColor = System.Drawing.Color.Chocolate;
            this.sessionsButton.Location = new System.Drawing.Point(134, 58);
            this.sessionsButton.Name = "sessionsButton";
            this.sessionsButton.Size = new System.Drawing.Size(100, 100);
            this.sessionsButton.TabIndex = 4;
            this.sessionsButton.Text = "Sessions";
            this.sessionsButton.UseVisualStyleBackColor = false;
            this.sessionsButton.Click += new System.EventHandler(this.sessionsButton_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(112, 9);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(72, 13);
            this.statusLabel.TabIndex = 6;
            this.statusLabel.Text = "Status: Admin";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(12, 9);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(41, 13);
            this.nameLabel.TabIndex = 5;
            this.nameLabel.Text = "Name: ";
            // 
            // settingsButton
            // 
            this.settingsButton.BackColor = System.Drawing.Color.Chocolate;
            this.settingsButton.Location = new System.Drawing.Point(190, 307);
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size(75, 23);
            this.settingsButton.TabIndex = 7;
            this.settingsButton.Text = "Settings";
            this.settingsButton.UseVisualStyleBackColor = false;
            this.settingsButton.Click += new System.EventHandler(this.settingsButton_Click);
            // 
            // AdminActionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(277, 337);
            this.Controls.Add(this.settingsButton);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.sessionsButton);
            this.Controls.Add(this.clientsButton);
            this.Controls.Add(this.subcriptionTypesButton);
            this.Controls.Add(this.sessionTypesButton);
            this.Controls.Add(this.button1);
            this.MaximumSize = new System.Drawing.Size(293, 376);
            this.MinimumSize = new System.Drawing.Size(293, 376);
            this.Name = "AdminActionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button sessionTypesButton;
        private System.Windows.Forms.Button subcriptionTypesButton;
        private System.Windows.Forms.Button clientsButton;
        private System.Windows.Forms.Button sessionsButton;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Button settingsButton;
    }
}