﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Clients
{
    public partial class ClientSessionsDialog : Form
    {
        Client client;
        Subscription subscription;

        public ClientSessionsDialog(Client client, Subscription subscription)
        {
            InitializeComponent();
            this.client = client;
            this.subscription = subscription;
            fetchSessions();
        }

        private void fetchSessions()
        {
            SessionViewErrorCatcher error = SessionsData.GetGroupSessionViews(subscription.SubscriptionType.SessionType.label, client.idClient);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                sessionsTable.DataSource = error.sessions;
                sessionsTable.Columns["idSession"].Visible = false;
                sessionsTable.Columns["idSessionType"].Visible = false;
            }
        }

        private void ClientSessionsDialog_Load(object sender, EventArgs e)
        {

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                return;
            }
            SessionView sessionView = (SessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }
            Session session = sessionError.session;

            ErrorCatcher error = ClientsData.SaveClientSession(client, subscription, session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void deleteClientSession(Client client, Session session)
        {
            ErrorCatcher error = ClientsData.DeleteClientSession(client, session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
        }
    }
}
