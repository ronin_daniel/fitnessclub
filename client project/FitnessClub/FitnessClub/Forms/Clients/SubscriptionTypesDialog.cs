﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Clients
{
    public partial class SubscriptionTypesDialog : Form
    {
        Subscription subscription;

        public SubscriptionTypesDialog(Subscription subscription)
        {
            InitializeComponent();
            this.subscription = subscription;
            subscriptionTypesTable.DataSource = SubscriptionTypesData.GetPricedSubscriptionTypes();
            subscriptionTypesTable.Columns["idSubscriptionType"].Visible = false;
            subscriptionTypesTable.Columns["idSessionType"].Visible = false;
        }

        private void SubscriptionTypesDialog_Load(object sender, EventArgs e)
        {

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (subscriptionTypesTable.SelectedRows[0].DataBoundItem == null)
            {
                return;
            }
            PricedSubscriptionType pricedSubscriptionType = (PricedSubscriptionType)subscriptionTypesTable.SelectedRows[0].DataBoundItem;
            SubscriptionTypeErrorCatcher error = SubscriptionTypesData.GetSubcriptionTypeById(pricedSubscriptionType.idSubscriptionType);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                subscription.SubscriptionType = error.subscriptionType;
                this.DialogResult = DialogResult.OK;
            }
            this.Close();
        }
    }
}
