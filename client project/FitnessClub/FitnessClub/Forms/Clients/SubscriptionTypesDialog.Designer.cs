﻿
namespace FitnessClub.Forms.Clients
{
    partial class SubscriptionTypesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.okButton = new System.Windows.Forms.Button();
            this.subscriptionTypesTable = new System.Windows.Forms.DataGridView();
            this.pricedSubscriptionTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.subscriptionTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypesTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pricedSubscriptionTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.okButton.BackColor = System.Drawing.Color.Chocolate;
            this.okButton.Location = new System.Drawing.Point(514, 128);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(65, 65);
            this.okButton.TabIndex = 14;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // subscriptionTypesTable
            // 
            this.subscriptionTypesTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.subscriptionTypesTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.subscriptionTypesTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.subscriptionTypesTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.subscriptionTypesTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.subscriptionTypesTable.Location = new System.Drawing.Point(13, 13);
            this.subscriptionTypesTable.MultiSelect = false;
            this.subscriptionTypesTable.Name = "subscriptionTypesTable";
            this.subscriptionTypesTable.ReadOnly = true;
            this.subscriptionTypesTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.subscriptionTypesTable.Size = new System.Drawing.Size(495, 303);
            this.subscriptionTypesTable.TabIndex = 13;
            // 
            // SubscriptionTypesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(591, 329);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.subscriptionTypesTable);
            this.Name = "SubscriptionTypesDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Subscription Types";
            this.Load += new System.EventHandler(this.SubscriptionTypesDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypesTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pricedSubscriptionTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.DataGridView subscriptionTypesTable;
        private System.Windows.Forms.BindingSource subscriptionTypeBindingSource;
        private System.Windows.Forms.BindingSource pricedSubscriptionTypeBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSubscriptionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sessionsAmountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn discountAsSubscriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
    }
}