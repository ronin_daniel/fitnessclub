﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Clients
{
    public partial class ClientsForm : Form
    {
        public ClientsForm()
        {
            InitializeComponent();
            fetchClients();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (clientsTable.SelectedRows.Count == 0 || clientsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a client");
                return;
            }
            ErrorCatcher error = ClientsData.DeleteClient((Client) clientsTable.SelectedRows[0].DataBoundItem);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchClients();
        }

        private void fetchClients()
        {
            clientsTable.DataSource = ClientsData.GetClients();
            clientsTable.Columns["idClient"].Visible = false;
            clientsTable.Columns["Subscriptions"].Visible = false;
            clientsTable.Columns["Sessions"].Visible = false;
        }

        private void addCoachButton_Click(object sender, EventArgs e)
        {
            ClientsEditorForm dialog = new ClientsEditorForm(new Client());
            DialogResult result = dialog.ShowDialog();
            fetchClients();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (clientsTable.SelectedRows.Count == 0 || clientsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a client");
                return;
            }
            ClientsEditorForm dialog = new ClientsEditorForm((Client)clientsTable.SelectedRows[0].DataBoundItem);
            dialog.ShowDialog();
            fetchClients();
        }

        private void ClientsForm_Load(object sender, EventArgs e)
        {

        }
    }
}
