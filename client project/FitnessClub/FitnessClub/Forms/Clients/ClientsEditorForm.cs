﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Clients
{
    public partial class ClientsEditorForm : Form
    {
        Client client;

        public ClientsEditorForm(Client client)
        {
            InitializeComponent();
            this.client = client;
            nameTextBox.Text = client.name;
            passwordTextBox.Text = client.password;
            numberTextBox.Text = client.number;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            ErrorCatcher error = ClientsData.SaveClient(client);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.Close();
            }
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            client.name = nameTextBox.Text;
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            client.password = passwordTextBox.Text;
        }

        private void numberTextBox_TextChanged(object sender, EventArgs e)
        {
            client.number = numberTextBox.Text;
        }
    }
}
