﻿
namespace FitnessClub.Forms.SessionTypes
{
    partial class SessionTypesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sessionTypesTable = new System.Windows.Forms.DataGridView();
            this.sessionTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.addCoachButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypesTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // sessionTypesTable
            // 
            this.sessionTypesTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionTypesTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sessionTypesTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.sessionTypesTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sessionTypesTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sessionTypesTable.Location = new System.Drawing.Point(12, 12);
            this.sessionTypesTable.MultiSelect = false;
            this.sessionTypesTable.Name = "sessionTypesTable";
            this.sessionTypesTable.ReadOnly = true;
            this.sessionTypesTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sessionTypesTable.Size = new System.Drawing.Size(348, 310);
            this.sessionTypesTable.TabIndex = 0;
            // 
            // editButton
            // 
            this.editButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editButton.BackColor = System.Drawing.Color.Chocolate;
            this.editButton.Location = new System.Drawing.Point(366, 247);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 75);
            this.editButton.TabIndex = 6;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = false;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.BackColor = System.Drawing.Color.Chocolate;
            this.deleteButton.Location = new System.Drawing.Point(366, 122);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 75);
            this.deleteButton.TabIndex = 5;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // addCoachButton
            // 
            this.addCoachButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addCoachButton.BackColor = System.Drawing.Color.Chocolate;
            this.addCoachButton.Location = new System.Drawing.Point(366, 12);
            this.addCoachButton.Name = "addCoachButton";
            this.addCoachButton.Size = new System.Drawing.Size(75, 75);
            this.addCoachButton.TabIndex = 4;
            this.addCoachButton.Text = "Add";
            this.addCoachButton.UseVisualStyleBackColor = false;
            this.addCoachButton.Click += new System.EventHandler(this.addCoachButton_Click);
            // 
            // SessionTypesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(453, 334);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addCoachButton);
            this.Controls.Add(this.sessionTypesTable);
            this.Name = "SessionTypesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Session Types";
            this.Load += new System.EventHandler(this.SessionTypesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypesTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView sessionTypesTable;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button addCoachButton;
        private System.Windows.Forms.BindingSource sessionTypeBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn labelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn peopleAmountDataGridViewTextBoxColumn;
    }
}