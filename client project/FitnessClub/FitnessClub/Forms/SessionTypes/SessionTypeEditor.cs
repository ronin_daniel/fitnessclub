﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.SessionTypes
{
    public partial class SessionTypeEditor : Form
    {
        private SessionType sessionType;

        public SessionTypeEditor(SessionType sessionType)
        {
            InitializeComponent();
            this.sessionType = sessionType;
            labelTextBox.Text = sessionType.label;
            priceNumeric.Value = sessionType.price;
            peopleNumeric.Value = sessionType.peopleAmount;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            ErrorCatcher error = SessionTypesData.SaveSessionType(sessionType);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.Close();
            }
        }

        private void labelTextBox_TextChanged(object sender, EventArgs e)
        {
            sessionType.label = labelTextBox.Text;
        }

        private void priceNumeric_ValueChanged(object sender, EventArgs e)
        {
            sessionType.price = priceNumeric.Value;
        }

        private void peopleNumeric_ValueChanged(object sender, EventArgs e)
        {
            sessionType.peopleAmount = (byte) peopleNumeric.Value;
        }

        private void SessionTypeEditor_Load(object sender, EventArgs e)
        {

        }
    }
}
