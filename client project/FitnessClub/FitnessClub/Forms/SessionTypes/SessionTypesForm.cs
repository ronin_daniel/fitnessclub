﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.SessionTypes
{
    public partial class SessionTypesForm : Form
    {
        public SessionTypesForm()
        {
            InitializeComponent();
            fetchTypes();
        }

        private void fetchTypes()
        {
            sessionTypesTable.DataSource = SessionTypesData.GetSessionTypes();
            sessionTypesTable.Columns["idSessionType"].Visible = false;
            sessionTypesTable.Columns["SubscriptionTypes"].Visible = false;
            sessionTypesTable.Columns["Sessions"].Visible = false;
            sessionTypesTable.Columns["Coaches"].Visible = false;
        }

        private void addCoachButton_Click(object sender, EventArgs e)
        {
            SessionTypeEditor form = new SessionTypeEditor(new SessionType());
            form.ShowDialog();
            fetchTypes();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (sessionTypesTable.SelectedRows.Count == 0 || sessionTypesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a session type");
                return;
            }
            SessionTypeEditor form = new SessionTypeEditor((SessionType)sessionTypesTable.SelectedRows[0].DataBoundItem);
            form.ShowDialog();
            fetchTypes();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (sessionTypesTable.SelectedRows.Count == 0 || sessionTypesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a session type");
                return;
            }
            ErrorCatcher error = SessionTypesData.DeleteSessionType((SessionType)sessionTypesTable.SelectedRows[0].DataBoundItem);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchTypes();
        }

        private void SessionTypesForm_Load(object sender, EventArgs e)
        {

        }
    }
}
