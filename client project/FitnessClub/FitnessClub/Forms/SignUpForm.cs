﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms
{
    public partial class SignUpForm : Form
    {
        public SignUpForm()
        {
            InitializeComponent();
        }

        private void signUpButton_Click(object sender, EventArgs e)
        {
            string name = nameTextBox.Text.Trim();
            string password = passwordTextBox.Text.Trim();
            string number = numberTextBox.Text.Trim();
            User user = Core.Context.Users.Where(u => u.name == name && u.password == password).FirstOrDefault();
            if (user == null)
            {
                Client client = new Client();
                client.name = name;
                client.password = password;
                client.number = number;
                ErrorCatcher error = ClientsData.SaveClient(client);
                this.Close();
            }
            else
            {
                MessageBox.Show("User with such credential already exists!");
            }
        }
    }
}
