﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.SusbcriptionTypes
{
    public partial class SubcriptionTypesForm : Form
    {
        public SubcriptionTypesForm()
        {
            InitializeComponent();
            fetchTypes();
        }

        private void fetchTypes()
        {
            subscriptionTypesTable.DataSource = SubscriptionTypesData.GetPricedSubscriptionTypes();
            subscriptionTypesTable.Columns["idSubscriptionType"].Visible = false;
            subscriptionTypesTable.Columns["idSessionType"].Visible = false;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (subscriptionTypesTable.SelectedRows.Count == 0 || subscriptionTypesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a type");
                return;
            }
            PricedSubscriptionType pricedSubscriptionType = (PricedSubscriptionType)subscriptionTypesTable.SelectedRows[0].DataBoundItem;
            SubscriptionTypeErrorCatcher subError = SubscriptionTypesData.GetSubcriptionTypeById(pricedSubscriptionType.idSubscriptionType);
            if (subError.isError())
            {
                MessageBox.Show(subError.message);
                return;
            }
            SubscriptionType subscriptionType = subError.subscriptionType;
            ErrorCatcher error = SubscriptionTypesData.DeleteSubscriptionType(subscriptionType);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchTypes();
        }

        private void addCoachButton_Click(object sender, EventArgs e)
        {
            SubscriptionTypeEditor dialog = new SubscriptionTypeEditor(new SubscriptionType());
            dialog.ShowDialog();
            fetchTypes();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (subscriptionTypesTable.SelectedRows.Count == 0 || subscriptionTypesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a type");
                return;
            }
            PricedSubscriptionType pricedSubscriptionType = (PricedSubscriptionType)subscriptionTypesTable.SelectedRows[0].DataBoundItem;
            SubscriptionTypeErrorCatcher error = SubscriptionTypesData.GetSubcriptionTypeById(pricedSubscriptionType.idSubscriptionType);
            if (error.isError())
            {
                MessageBox.Show(error.message);
                return;
            }
            SubscriptionType subscriptionType = error.subscriptionType;
            SubscriptionTypeEditor dialog = new SubscriptionTypeEditor(subscriptionType);
            dialog.ShowDialog();
            fetchTypes();
        }

        private void SubcriptionTypesForm_Load(object sender, EventArgs e)
        {

        }
    }
}
