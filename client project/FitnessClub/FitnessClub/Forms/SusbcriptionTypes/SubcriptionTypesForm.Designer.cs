﻿
namespace FitnessClub.Forms.SusbcriptionTypes
{
    partial class SubcriptionTypesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.subscriptionTypesTable = new System.Windows.Forms.DataGridView();
            this.subscriptionTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.addCoachButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypesTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // subscriptionTypesTable
            // 
            this.subscriptionTypesTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.subscriptionTypesTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.subscriptionTypesTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.subscriptionTypesTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.subscriptionTypesTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.subscriptionTypesTable.Location = new System.Drawing.Point(12, 12);
            this.subscriptionTypesTable.MultiSelect = false;
            this.subscriptionTypesTable.Name = "subscriptionTypesTable";
            this.subscriptionTypesTable.ReadOnly = true;
            this.subscriptionTypesTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.subscriptionTypesTable.Size = new System.Drawing.Size(508, 310);
            this.subscriptionTypesTable.TabIndex = 0;
            // 
            // editButton
            // 
            this.editButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editButton.BackColor = System.Drawing.Color.Chocolate;
            this.editButton.Enabled = false;
            this.editButton.Location = new System.Drawing.Point(526, 127);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 75);
            this.editButton.TabIndex = 12;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = false;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteButton.BackColor = System.Drawing.Color.Chocolate;
            this.deleteButton.Location = new System.Drawing.Point(526, 247);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 75);
            this.deleteButton.TabIndex = 11;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // addCoachButton
            // 
            this.addCoachButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addCoachButton.BackColor = System.Drawing.Color.Chocolate;
            this.addCoachButton.Location = new System.Drawing.Point(526, 12);
            this.addCoachButton.Name = "addCoachButton";
            this.addCoachButton.Size = new System.Drawing.Size(75, 75);
            this.addCoachButton.TabIndex = 10;
            this.addCoachButton.Text = "Add";
            this.addCoachButton.UseVisualStyleBackColor = false;
            this.addCoachButton.Click += new System.EventHandler(this.addCoachButton_Click);
            // 
            // SubcriptionTypesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(613, 334);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addCoachButton);
            this.Controls.Add(this.subscriptionTypesTable);
            this.Name = "SubcriptionTypesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Subcription Types";
            this.Load += new System.EventHandler(this.SubcriptionTypesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypesTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionTypeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView subscriptionTypesTable;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button addCoachButton;
        private System.Windows.Forms.BindingSource subscriptionTypeBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSubscriptionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sessionsAmountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn discountAsSubscriptionDataGridViewTextBoxColumn;
    }
}