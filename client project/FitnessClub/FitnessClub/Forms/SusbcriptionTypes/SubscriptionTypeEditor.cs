﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.SusbcriptionTypes
{
    public partial class SubscriptionTypeEditor : Form
    {
        SubscriptionType subscriptionType;

        public SubscriptionTypeEditor(SubscriptionType subscriptionType)
        {
            InitializeComponent();
            this.subscriptionType = subscriptionType;
            sessionTypeComboBox.DataSource = SessionTypesData.GetSessionTypes();
            sessionTypeComboBox.DisplayMember = "label";
            sessionTypeComboBox.SelectedItem = subscriptionType.SessionType;
            sessionsNumberic.Value = subscriptionType.sessionsAmount;
            discountNumeric.Value = subscriptionType.discountAsSubscription * 100;
        }

        private void sessionTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sessionsNumberic_ValueChanged(object sender, EventArgs e)
        {
            subscriptionType.sessionsAmount = (int) sessionsNumberic.Value;
        }

        private void discountNumeric_ValueChanged(object sender, EventArgs e)
        {
            subscriptionType.discountAsSubscription = discountNumeric.Value / 100;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (((SessionType)sessionTypeComboBox.SelectedItem) == null)
                {
                    MessageBox.Show("Fill in all the fields");
                    return;
                }
                subscriptionType.idSessionType = ((SessionType)sessionTypeComboBox.SelectedItem).idSessionType;

                ErrorCatcher error = SubscriptionTypesData.SaveSubscriptionType(subscriptionType);
                if (error.isError())
                {
                    MessageBox.Show(error.message);
                }
                else
                {
                    this.Close();
                }
            } 
            catch
            {

            }
        }

        private void SubscriptionTypeEditor_Load(object sender, EventArgs e)
        {

        }
    }
}
