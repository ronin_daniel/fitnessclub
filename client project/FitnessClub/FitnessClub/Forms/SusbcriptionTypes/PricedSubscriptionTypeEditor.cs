﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.SusbcriptionTypes
{
    public partial class PricedSubscriptionTypeEditor : Form
    {
        PricedSubscriptionType subscriptionType;

        public PricedSubscriptionTypeEditor(PricedSubscriptionType subscriptionType)
        {
            InitializeComponent();
            this.subscriptionType = subscriptionType;
            sessionTypeComboBox.DataSource = SessionTypesData.GetSessionTypes();
            sessionTypeComboBox.DisplayMember = "label";
            SessionTypeErrorCatcher error = SessionTypesData.GetSessionType(subscriptionType.idSessionType);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            sessionTypeComboBox.SelectedItem = error.sessionType;
            sessionsNumberic.Value = subscriptionType.sessionsAmount;
            discountNumeric.Value = subscriptionType.Discount * 100;
        }

        private void sessionsNumberic_ValueChanged_1(object sender, EventArgs e)
        {
            subscriptionType.sessionsAmount = (int)sessionsNumberic.Value;
        }

        private void discountNumeric_ValueChanged_1(object sender, EventArgs e)
        {
            subscriptionType.Discount = discountNumeric.Value / 100;
        }

        private void okButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (((SessionType)sessionTypeComboBox.SelectedItem) == null)
                {
                    MessageBox.Show("Fill in all the fields");
                    return;
                }
                subscriptionType.idSessionType = ((SessionType)sessionTypeComboBox.SelectedItem).idSessionType;

                ErrorCatcher error = SubscriptionTypesData.SavePricedSubscriptionType(subscriptionType);
                if (error.isError())
                {
                    MessageBox.Show(error.message);
                }
                else
                {
                    this.Close();
                }
            }
            catch
            {

            }
        }
    }
}
