﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessClub.Forms.Coaches
{
    public partial class CoachEditorForm : Form
    {
        Coach coach;

        public CoachEditorForm(Coach coach)
        {
            InitializeComponent();
            this.coach = coach;
            nameTextBox.Text = coach.name;
            passwordTextBox.Text = coach.password;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            ErrorCatcher error = CoachesData.SaveCoach(coach);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.Close();
            }
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            coach.name = nameTextBox.Text;
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {
            coach.password = passwordTextBox.Text;
        }
    }
}
