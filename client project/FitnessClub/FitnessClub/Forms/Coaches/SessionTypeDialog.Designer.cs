﻿
namespace FitnessClub.Forms.Coaches
{
    partial class SessionTypeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.okButton = new System.Windows.Forms.Button();
            this.sessionTypesTable = new System.Windows.Forms.DataGridView();
            this.sessionTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypesTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.okButton.BackColor = System.Drawing.Color.Chocolate;
            this.okButton.Location = new System.Drawing.Point(454, 93);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 75);
            this.okButton.TabIndex = 8;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // sessionTypesTable
            // 
            this.sessionTypesTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionTypesTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sessionTypesTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.sessionTypesTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sessionTypesTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sessionTypesTable.Location = new System.Drawing.Point(12, 12);
            this.sessionTypesTable.MultiSelect = false;
            this.sessionTypesTable.Name = "sessionTypesTable";
            this.sessionTypesTable.ReadOnly = true;
            this.sessionTypesTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sessionTypesTable.Size = new System.Drawing.Size(436, 250);
            this.sessionTypesTable.TabIndex = 7;
            // 
            // SessionTypeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(541, 278);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.sessionTypesTable);
            this.Name = "SessionTypeDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Session Types";
            this.Load += new System.EventHandler(this.SessionTypeDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypesTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionTypeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.DataGridView sessionTypesTable;
        private System.Windows.Forms.BindingSource sessionTypeBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn labelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn peopleAmountDataGridViewTextBoxColumn;
    }
}