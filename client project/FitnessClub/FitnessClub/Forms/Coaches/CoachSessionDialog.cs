﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Coaches
{
    public partial class CoachSessionDialog : Form
    {
        Coach coach;

        public CoachSessionDialog(Coach coach)
        {
            InitializeComponent();
            this.coach = coach;
            fetchSessions();
        }

        private void fetchSessions()
        {
            SessionViewErrorCatcher error = SessionsData.GetSessionViews(coach.idCoach);
            if (error.isError())
            {
                MessageBox.Show(error.message);
                return;
            }
            sessionsTable.DataSource = error.sessions;
            sessionsTable.Columns["idSession"].Visible = false;
            sessionsTable.Columns["idSessionType"].Visible = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                return;
            }

            SessionView sessionView = (SessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }
            Session session = sessionError.session;

            ErrorCatcher error = SessionsData.ArrangeCoach(session, coach);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void CoachSessionDialog_Load(object sender, EventArgs e)
        {

        }

        private void sessionsTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
