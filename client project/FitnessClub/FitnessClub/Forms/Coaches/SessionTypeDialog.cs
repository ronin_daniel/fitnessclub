﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;

namespace FitnessClub.Forms.Coaches
{
    public partial class SessionTypeDialog : Form
    {
        Coach coach;
        bool delMode;

        public SessionTypeDialog(Coach coach, bool delMode)
        {
            InitializeComponent();
            this.coach = coach;
            this.delMode = delMode;
            if (delMode)
            {
                SessionTypesErrorCatcher error = SessionTypesData.GetCoachSessionTypes(coach.idCoach);
                if (error.isError())
                {
                    MessageBox.Show(error.message);
                }
                else
                {
                    sessionTypesTable.DataSource = error.sessionTypes;
                }
            }
            else
            {
                sessionTypesTable.DataSource = SessionTypesData.GetSessionTypes();
            }
            refineColumns();
        }

        private void refineColumns()
        {
            sessionTypesTable.Columns["idSessionType"].Visible = false;
            sessionTypesTable.Columns["Sessions"].Visible = false;
            sessionTypesTable.Columns["Coaches"].Visible = false;
            sessionTypesTable.Columns["SubscriptionTypes"].Visible = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            ErrorCatcher error;
            if (delMode)
            {
                error = CoachesData.DettachSessionType(coach, (SessionType)sessionTypesTable.SelectedRows[0].DataBoundItem);
            }
            else
            {
                error = CoachesData.AttachSessionType(coach, (SessionType)sessionTypesTable.SelectedRows[0].DataBoundItem);
            }
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void SessionTypeDialog_Load(object sender, EventArgs e)
        {

        }
    }
}
