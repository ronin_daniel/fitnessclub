﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessClub.Forms.Coaches
{
    public partial class Coaches : Form
    {
        public Coaches()
        {
            InitializeComponent();
            fetchCoaches();
        }

        private void addCoachButton_Click(object sender, EventArgs e)
        {
            CoachEditorForm form = new CoachEditorForm(new Coach());
            form.ShowDialog();
            fetchCoaches();
        }

        private void fetchCoaches()
        {
            coachesTable.DataSource = CoachesData.GetCoaches();
            coachesTable.Columns["idCoach"].Visible = false;
            coachesTable.Columns["Sessions"].Visible = false;
            coachesTable.Columns["SessionTypes"].Visible = false;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (coachesTable.SelectedRows.Count == 0 || coachesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a coach");
                return;
            }
            CoachEditorForm form = new CoachEditorForm((Coach) coachesTable.SelectedRows[0].DataBoundItem);
            form.ShowDialog();
            fetchCoaches();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (coachesTable.SelectedRows.Count == 0 || coachesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a coach");
                return;
            }

            ErrorCatcher error = CoachesData.DeleteCoach((Coach) coachesTable.SelectedRows[0].DataBoundItem);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchCoaches();
        }

        private void sessionTypeButton_Click(object sender, EventArgs e)
        {
            if (coachesTable.SelectedRows.Count == 0 || coachesTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Pick a coach");
                return;
            }
            SessionTypeDialog dialog = new SessionTypeDialog((Coach)coachesTable.SelectedRows[0].DataBoundItem, false);
            dialog.ShowDialog();
            fetchCoaches();
        }

        private void CoachesForm_Load(object sender, EventArgs e)
        {

        }
    }
}
