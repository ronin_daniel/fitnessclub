﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FitnessClub.Models;
using FitnessClub.Forms.Clients;
using FitnessClub.Forms.Sessions;
using Microsoft.Office.Interop.Word;
using word = Microsoft.Office.Interop.Word;

namespace FitnessClub.Forms
{
    public partial class ClientActionsForm : Form
    {
        Client client;

        public ClientActionsForm()
        {
            InitializeComponent();
            ClientErrorCatcher error = ClientsData.GetClientByCurrentUser();
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                client = error.client;
            }
            fetchSessions();
            fetchSubscriptions();
            nameLabel.Text = "Name: "+ CurrentUser.name;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ClientActionsForm_Load(object sender, EventArgs e)
        {

        }

        private void fetchSessions()
        {
            ClientSessionViewErrorCatcher error = SessionsData.GetClientSessionViews(client.idClient);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                sessionsTable.DataSource = error.sessions;
                sessionsTable.Columns["idSession"].Visible = false;
                sessionsTable.Columns["idClient"].Visible = false;
            }
        }

        private void fetchSubscriptions()
        {
            AmountSubscriptionErrorCatcher error = ClientsData.GetAmountSubscriptions(client);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            else
            {
                subscriptionsTable.DataSource = error.amountSubscription;
                subscriptionsTable.Columns["idSubscription"].Visible = false;
                subscriptionsTable.Columns["idClient"].Visible = false;
                subscriptionsTable.Columns["idSessionType"].Visible = false;
            }
        }

        private void arrangeSessionButton_Click(object sender, EventArgs e)
        {
            if (subscriptionsTable.SelectedRows.Count == 0 || subscriptionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Select a subscription");
                return;
            }

            AmountSubscription amountSubscription = (AmountSubscription)subscriptionsTable.SelectedRows[0].DataBoundItem;
            SubscriptionErrorCatcher subErro = ClientsData.GetSubscription(amountSubscription.idSubscription);
            if (subErro.isError())
            {
                MessageBox.Show(subErro.message);
                return;
            }

            bool isSoloSession = subErro.subscription.SubscriptionType.SessionType.peopleAmount == 1;
            if (isSoloSession)
            {
                MessageBox.Show("This subscription contains solo sessions, group session can't be purchased!");
                return;
            }

            ClientSessionsDialog dialog = new ClientSessionsDialog(client, subErro.subscription);
            dialog.ShowDialog();
            fetchSessions();
            fetchSubscriptions();
        }

        private void BuytSubscriptionButton_Click(object sender, EventArgs e)
        {
            Subscription subscription = new Subscription();
            SubscriptionTypesDialog dialog = new SubscriptionTypesDialog(subscription);
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }

            ErrorCatcher error = ClientsData.SaveSubscription(subscription, client);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchSubscriptions();
        }

        private void deleteSession(Session session)
        {
            ErrorCatcher error = SessionsData.DeleteSession(session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
        }

        private void SoloSessionButton_Click(object sender, EventArgs e)
        {
            if (subscriptionsTable.SelectedRows.Count == 0 ||subscriptionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Select a subscription");
                return;
            }

            AmountSubscription amountSubscription = (AmountSubscription)subscriptionsTable.SelectedRows[0].DataBoundItem;
            SubscriptionErrorCatcher subErro = ClientsData.GetSubscription(amountSubscription.idSubscription);
            if (subErro.isError())
            {
                MessageBox.Show(subErro.message);
                return;
            }

            bool isGroupSession = subErro.subscription.SubscriptionType.SessionType.peopleAmount > 1;
            if (isGroupSession)
            {
                MessageBox.Show("This subscription contains group sessions, solo session can't be purchased!");
                return;
            }

            List<SessionType> sessionType = new List<SessionType>();
            sessionType.Add(subErro.subscription.SubscriptionType.SessionType);
            Session session = new Session();
            SessionEditorForm dialog = new SessionEditorForm(session, sessionType);
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.OK)
            {
                deleteSession(session);
                return;
            }

            ErrorCatcher error1 = ClientsData.SaveClientSession(client, subErro.subscription, session);
            if (error1.isError())
            {
                deleteSession(session);
                MessageBox.Show(error1.message);
                return;
            }

            result = MessageBox.Show("Get a coach for the session?", "Coach", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CoachesDialog coachDialog = new CoachesDialog(session, session.SessionType);
                coachDialog.ShowDialog();
            }
            
            fetchSessions();
            fetchSubscriptions();
        }

        private void deleteSubscriptionSession(Subscription subscription, Session session)
        {
            ErrorCatcher error = ClientsData.DeleteSubscriptionSession(subscription, session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
        }

        private void subscriptionsTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (subscriptionsTable.SelectedRows.Count == 0 || subscriptionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Select a subscription");
                return;
            }

            ErrorCatcher error = ClientsData.DeleteSubscription(((AmountSubscription)subscriptionsTable.SelectedRows[0].DataBoundItem).idSubscription);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchSubscriptions();
        }

        private void deleteSessionButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Select a session");
                return;
            }

            ClientSessionView sessionView = (ClientSessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }

            ErrorCatcher error = ClientsData.DeleteClientSession(client, sessionError.session);
            if (error.isError())
            {
                MessageBox.Show(error.message);
            }
            fetchSessions();
            fetchSubscriptions();
        }

        private void arrangeCoachButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Select a session");
                return;
            }

            ClientSessionView sessionView = (ClientSessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }

            Session pickedSession = sessionError.session;
            bool isGroupSession = pickedSession.SessionType.peopleAmount > 1;
            if (isGroupSession)
            {
                MessageBox.Show("Select solo session with 1 person. Group sessions' coaches assigned only by administration!");
                return;
            }

            CoachesDialog coachDialog = new CoachesDialog(pickedSession, false);
            coachDialog.ShowDialog();

            fetchSessions();
            fetchSubscriptions();
        }

        private void detachCoachButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Select a session");
                return;
            }

            ClientSessionView sessionView = (ClientSessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }

            Session pickedSession = sessionError.session;
            bool isGroupSession = pickedSession.SessionType.peopleAmount > 1;
            if (isGroupSession)
            {
                MessageBox.Show("Select solo session with 1 person. Group sessions' coaches assigned only by administration!");
                return;
            }

            CoachesDialog coachDialog = new CoachesDialog(pickedSession, true);
            coachDialog.ShowDialog();

            fetchSessions();
            fetchSubscriptions();
        }

        private void editSessionButton_Click(object sender, EventArgs e)
        {
            if (sessionsTable.SelectedRows.Count == 0 || sessionsTable.SelectedRows[0].DataBoundItem == null)
            {
                MessageBox.Show("Select a session");
                return;
            }

            ClientSessionView sessionView = (ClientSessionView)sessionsTable.SelectedRows[0].DataBoundItem;
            SessionErrorCatcher sessionError = SessionsData.GetSession(sessionView.idSession);
            if (sessionError.isError())
            {
                MessageBox.Show(sessionError.message);
                return;
            }

            Session pickedSession = sessionError.session;
            bool isGroupSession = pickedSession.SessionType.peopleAmount > 1;
            if (isGroupSession)
            {
                MessageBox.Show("Select solo session with 1 person. Group sessions edited only by administration!");
                return;
            }

            List<SessionType> sessionType = new List<SessionType>();
            sessionType.Add(pickedSession.SessionType);
            SessionEditorForm dialog = new SessionEditorForm(pickedSession, sessionType);
            DialogResult result = dialog.ShowDialog();

            fetchSessions();
            fetchSubscriptions();
        }

        private void printSubsButton_Click(object sender, EventArgs e)
        {
            try
            {
                string filename = "";
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "docx|*.docx";
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    filename = dialog.FileName;
                }
                else
                {
                    return;
                }

                List<PricedSubscriptionType> types = SubscriptionTypesData.GetPricedSubscriptionTypes();
                var wordApp = new word.Application();
                wordApp.Visible = true;
                var document = wordApp.Documents.Add();
                var paragraph = document.Paragraphs.Add();
                var range = paragraph.Range;
                var table = document.Tables.Add(range, types.Count + 1, 4);
                table.Borders.InsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                table.Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                table.AllowAutoFit = true;
                table.Cell(1, 1).Range.Text = "SessionsAmount";
                table.Cell(1, 2).Range.Text = "Discount";
                table.Cell(1, 3).Range.Text = "Price";
                table.Cell(1, 4).Range.Text = "SessionType";

                for (int i = 2; i <= table.Rows.Count; i++)
                {
                    for (int j = 1; j <= table.Columns.Count; j++)
                    {
                        if (j == 1)
                        {
                            table.Cell(i, j).Range.Text = types[i - 2].sessionsAmount.ToString();
                        }
                        else if (j == 2)
                        {
                            table.Cell(i, j).Range.Text = types[i - 2].Discount.ToString();
                        }
                        else if (j == 3)
                        {
                            table.Cell(i, j).Range.Text = types[i - 2].Price.ToString();
                        }
                        else if (j == 4)
                        {
                            table.Cell(i, j).Range.Text = types[i - 2].SessionType.ToString();
                        }
                    }
                }
                document.SaveAs(filename);
            }
            catch
            {

            }
        }

        private void printSessionsButton_Click(object sender, EventArgs e)
        {
            
            try
            {
                string filename = "";
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "docx|*.docx";
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    filename = dialog.FileName;
                }
                else
                {
                    return;
                }

                List<ClientSessionView> sessions = null;
                ClientSessionViewErrorCatcher error = SessionsData.GetClientSessionViews(client.idClient);
                if (error.isError())
                {
                    return;
                }
                else
                {
                    sessions = error.sessions;
                }
                var wordApp = new word.Application();
                wordApp.Visible = true;
                var document = wordApp.Documents.Add();
                var paragraph = document.Paragraphs.Add();
                var range = paragraph.Range;
                var table = document.Tables.Add(range, sessions.Count + 1, 5);
                table.Borders.InsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                table.Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
                table.AllowAutoFit = true;
                table.Cell(1, 1).Range.Text = "ClientName";
                table.Cell(1, 2).Range.Text = "StartTime";
                table.Cell(1, 3).Range.Text = "EndTime";
                table.Cell(1, 4).Range.Text = "SessionType";
                table.Cell(1, 5).Range.Text = "PeopleAmount";

                for (int i = 2; i <= table.Rows.Count; i++)
                {
                    for (int j = 1; j <= table.Columns.Count; j++)
                    {
                        if (j == 1)
                        {
                            table.Cell(i, j).Range.Text = client.name;
                        }
                        else if (j == 2)
                        {
                            table.Cell(i, j).Range.Text = sessions[i - 2].StartTime.ToString();
                        }
                        else if (j == 3)
                        {
                            table.Cell(i, j).Range.Text = sessions[i - 2].EndTime.ToString();
                        }
                        else if (j == 4)
                        {
                            table.Cell(i, j).Range.Text = sessions[i - 2].SessionType.ToString();
                        }
                        else if (j == 5)
                        {
                            table.Cell(i, j).Range.Text = sessions[i - 2].PeopleAmount.ToString();
                        }
                    }
                }
                document.SaveAs(filename);
            }
            catch
            {

            }
        }
    }
}
