﻿
namespace FitnessClub.Forms
{
    partial class ClientActionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nameLabel = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.groupSessionButton = new System.Windows.Forms.Button();
            this.sessionsTable = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.subscriptionsTable = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.deleteSessionButton = new System.Windows.Forms.Button();
            this.arrangeCoachButton = new System.Windows.Forms.Button();
            this.detachCoachButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.editSessionButton = new System.Windows.Forms.Button();
            this.printSubsButton = new System.Windows.Forms.Button();
            this.sessionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.printSessionsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sessionsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(12, 9);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(41, 13);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name: ";
            this.nameLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(112, 9);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(69, 13);
            this.statusLabel.TabIndex = 1;
            this.statusLabel.Text = "Status: Client";
            // 
            // groupSessionButton
            // 
            this.groupSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupSessionButton.AutoSize = true;
            this.groupSessionButton.BackColor = System.Drawing.Color.Chocolate;
            this.groupSessionButton.Location = new System.Drawing.Point(15, 330);
            this.groupSessionButton.Name = "groupSessionButton";
            this.groupSessionButton.Size = new System.Drawing.Size(144, 23);
            this.groupSessionButton.TabIndex = 15;
            this.groupSessionButton.Text = "Purchase group session";
            this.groupSessionButton.UseVisualStyleBackColor = false;
            this.groupSessionButton.Click += new System.EventHandler(this.arrangeSessionButton_Click);
            // 
            // sessionsTable
            // 
            this.sessionsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sessionsTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.sessionsTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sessionsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sessionsTable.Location = new System.Drawing.Point(15, 45);
            this.sessionsTable.MultiSelect = false;
            this.sessionsTable.Name = "sessionsTable";
            this.sessionsTable.ReadOnly = true;
            this.sessionsTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.sessionsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sessionsTable.Size = new System.Drawing.Size(511, 250);
            this.sessionsTable.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Chocolate;
            this.button1.Location = new System.Drawing.Point(829, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Buy subscription";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.BuytSubscriptionButton_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.AutoSize = true;
            this.button2.BackColor = System.Drawing.Color.Chocolate;
            this.button2.Location = new System.Drawing.Point(15, 301);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(144, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Purchase solo session";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.SoloSessionButton_Click);
            // 
            // subscriptionsTable
            // 
            this.subscriptionsTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.subscriptionsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.subscriptionsTable.BackgroundColor = System.Drawing.Color.LightCoral;
            this.subscriptionsTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.subscriptionsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.subscriptionsTable.Location = new System.Drawing.Point(571, 45);
            this.subscriptionsTable.MultiSelect = false;
            this.subscriptionsTable.Name = "subscriptionsTable";
            this.subscriptionsTable.ReadOnly = true;
            this.subscriptionsTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.subscriptionsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.subscriptionsTable.Size = new System.Drawing.Size(402, 250);
            this.subscriptionsTable.TabIndex = 18;
            this.subscriptionsTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.subscriptionsTable_CellContentClick);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.Chocolate;
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(829, 330);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(144, 23);
            this.button3.TabIndex = 19;
            this.button3.Text = "Delete subscription";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // deleteSessionButton
            // 
            this.deleteSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSessionButton.BackColor = System.Drawing.Color.Chocolate;
            this.deleteSessionButton.Location = new System.Drawing.Point(383, 330);
            this.deleteSessionButton.Name = "deleteSessionButton";
            this.deleteSessionButton.Size = new System.Drawing.Size(144, 23);
            this.deleteSessionButton.TabIndex = 20;
            this.deleteSessionButton.Text = "Cancel session";
            this.deleteSessionButton.UseVisualStyleBackColor = false;
            this.deleteSessionButton.Click += new System.EventHandler(this.deleteSessionButton_Click);
            // 
            // arrangeCoachButton
            // 
            this.arrangeCoachButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.arrangeCoachButton.AutoSize = true;
            this.arrangeCoachButton.BackColor = System.Drawing.Color.Chocolate;
            this.arrangeCoachButton.Location = new System.Drawing.Point(206, 301);
            this.arrangeCoachButton.Name = "arrangeCoachButton";
            this.arrangeCoachButton.Size = new System.Drawing.Size(144, 23);
            this.arrangeCoachButton.TabIndex = 21;
            this.arrangeCoachButton.Text = "Arrange coach";
            this.arrangeCoachButton.UseVisualStyleBackColor = false;
            this.arrangeCoachButton.Click += new System.EventHandler(this.arrangeCoachButton_Click);
            // 
            // detachCoachButton
            // 
            this.detachCoachButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.detachCoachButton.AutoSize = true;
            this.detachCoachButton.BackColor = System.Drawing.Color.Chocolate;
            this.detachCoachButton.Location = new System.Drawing.Point(206, 330);
            this.detachCoachButton.Name = "detachCoachButton";
            this.detachCoachButton.Size = new System.Drawing.Size(144, 23);
            this.detachCoachButton.TabIndex = 22;
            this.detachCoachButton.Text = "Detach coach";
            this.detachCoachButton.UseVisualStyleBackColor = false;
            this.detachCoachButton.Click += new System.EventHandler(this.detachCoachButton_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 16);
            this.label1.TabIndex = 23;
            this.label1.Text = "SESSIONS";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(568, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 16);
            this.label2.TabIndex = 24;
            this.label2.Text = "SUBSCRIPTIONS";
            // 
            // editSessionButton
            // 
            this.editSessionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editSessionButton.BackColor = System.Drawing.Color.Chocolate;
            this.editSessionButton.Enabled = false;
            this.editSessionButton.Location = new System.Drawing.Point(383, 301);
            this.editSessionButton.Name = "editSessionButton";
            this.editSessionButton.Size = new System.Drawing.Size(144, 23);
            this.editSessionButton.TabIndex = 25;
            this.editSessionButton.Text = "Reschedule solo session";
            this.editSessionButton.UseVisualStyleBackColor = false;
            this.editSessionButton.Click += new System.EventHandler(this.editSessionButton_Click);
            // 
            // printSubsButton
            // 
            this.printSubsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.printSubsButton.BackColor = System.Drawing.Color.Chocolate;
            this.printSubsButton.Location = new System.Drawing.Point(679, 4);
            this.printSubsButton.Name = "printSubsButton";
            this.printSubsButton.Size = new System.Drawing.Size(144, 23);
            this.printSubsButton.TabIndex = 26;
            this.printSubsButton.Text = "Print subscription types";
            this.printSubsButton.UseVisualStyleBackColor = false;
            this.printSubsButton.Click += new System.EventHandler(this.printSubsButton_Click);
            // 
            // printSessionsButton
            // 
            this.printSessionsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.printSessionsButton.BackColor = System.Drawing.Color.Chocolate;
            this.printSessionsButton.Location = new System.Drawing.Point(829, 4);
            this.printSessionsButton.Name = "printSessionsButton";
            this.printSessionsButton.Size = new System.Drawing.Size(144, 23);
            this.printSessionsButton.TabIndex = 27;
            this.printSessionsButton.Text = "Print sessions";
            this.printSessionsButton.UseVisualStyleBackColor = false;
            this.printSessionsButton.Click += new System.EventHandler(this.printSessionsButton_Click);
            // 
            // ClientActionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.ClientSize = new System.Drawing.Size(985, 403);
            this.Controls.Add(this.printSessionsButton);
            this.Controls.Add(this.printSubsButton);
            this.Controls.Add(this.editSessionButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.detachCoachButton);
            this.Controls.Add(this.arrangeCoachButton);
            this.Controls.Add(this.deleteSessionButton);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.subscriptionsTable);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupSessionButton);
            this.Controls.Add(this.sessionsTable);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.nameLabel);
            this.Name = "ClientActionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Client";
            this.Load += new System.EventHandler(this.ClientActionsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sessionsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subscriptionsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Button groupSessionButton;
        private System.Windows.Forms.DataGridView sessionsTable;
        private System.Windows.Forms.BindingSource sessionBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSessionTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView subscriptionsTable;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button deleteSessionButton;
        private System.Windows.Forms.Button arrangeCoachButton;
        private System.Windows.Forms.Button detachCoachButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button editSessionButton;
        private System.Windows.Forms.Button printSubsButton;
        private System.Windows.Forms.Button printSessionsButton;
    }
}