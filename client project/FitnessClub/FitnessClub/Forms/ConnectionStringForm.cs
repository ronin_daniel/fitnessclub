﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FitnessClub.Forms
{
    public partial class ConnectionStringForm : Form
    {
        public ConnectionStringForm()
        {
            InitializeComponent();
            dbTextBox.Text = Properties.Settings.Default.Db;
            loginTextBox.Text = Properties.Settings.Default.login;
            passwordTextBox.Text = Properties.Settings.Default.password;
            serverTextBox.Text = Properties.Settings.Default.server;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default["Db"] = dbTextBox.Text;
            Properties.Settings.Default["login"] = loginTextBox.Text;
            Properties.Settings.Default["password"] = passwordTextBox.Text;
            Properties.Settings.Default["server"] = serverTextBox.Text;
            Properties.Settings.Default.Save();
            Core.ReconnectDatabase();
            this.Close();
        }

        private void serverTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
