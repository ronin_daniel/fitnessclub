﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClub
{
    public class ErrorCatcher
    {
        public string message;

        public void setMessage(Exception exception)
        {
            if (exception.InnerException == null)
            {
                this.message = exception.Message;
            }
            else
            {
                setMessage(exception.InnerException);
            }
        }

        public bool isError()
        {
            return !String.IsNullOrEmpty(message);
        }
    }
}
