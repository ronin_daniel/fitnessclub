﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClub.Models
{
    public static class SessionsData
    {
        public static List<Session> GetSessions()
        {
            return Core.Context.Sessions.ToList();
        }

        public static SessionErrorCatcher GetSession(int idSession)
        {
            SessionErrorCatcher error = new SessionErrorCatcher();
            try
            {
                error.session = Core.Context.Sessions.Where(s => s.idSession == idSession).FirstOrDefault();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static List<Session> GetGroupSessions(Client client)
        {
            return
                Core.Context.Sessions
                .Where(s => s.SessionType.peopleAmount > 1 
                            && s.Clients
                            .Where(c => c.idClient == client.idClient).ToList().Count == 0).ToList();
        }

        public static List<SessionView> GetSessionsView()
        {
            return Core.Context.SessionViews.ToList();
        }

        public static CoachSessionErrorCatcher GetCoachSessions(int idCoach)
        {
            CoachSessionErrorCatcher error = new CoachSessionErrorCatcher();
            try
            {
                error.sessions =
                    Core.Context.CoachSessionViews
                    .Where(s => s.idCoach == idCoach)
                    .ToList();
                foreach (CoachSessionView coachSession in error.sessions)
                {
                    Core.Context.Entry(coachSession).Reload();
                }
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static CoachSessionErrorCatcher GetCoachSessions()
        {
            CoachSessionErrorCatcher error = new CoachSessionErrorCatcher();
            try
            {
                error.sessions =
                    Core.Context.CoachSessionViews
                    .ToList();
                foreach (CoachSessionView coachSession in error.sessions)
                {
                    Core.Context.Entry(coachSession).Reload();
                }
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static SessionsErrorCatcher GetClientSessions(int clientId)
        {
            SessionsErrorCatcher error = new SessionsErrorCatcher();
            try
            {
                error.sessions =
                    Core.Context.Sessions
                    .Where(s => s.Clients.Where(c => c.idClient == clientId).ToList().Count() > 0)
                    .ToList();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ClientSessionViewErrorCatcher GetClientSessionViews(int clientId)
        {
            ClientSessionViewErrorCatcher error = new ClientSessionViewErrorCatcher();
            try
            {
                error.sessions =
                    Core.Context.ClientSessionViews
                    .Where(s => s.idClient == clientId)
                    .ToList();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static SessionViewErrorCatcher GetGroupSessionViews(string sessionTypeLabel, int idClient)
        {
            SessionViewErrorCatcher error = new SessionViewErrorCatcher();
            try
            {
                error.sessions =
                    Core.Context.SessionViews
                    .Where(s => s.SessionType.Equals(sessionTypeLabel))
                    .ToList();

                List<SessionView> sessions = new List<SessionView>();
                foreach (SessionView sessionView in error.sessions)
                {
                    Session session = Core.Context.Sessions.Where(s => s.idSession == sessionView.idSession).FirstOrDefault();
                    bool isInSession = session.Clients.Where(c => c.idClient == idClient).ToList().Count > 0;
                    if (!isInSession)
                    {
                        sessions.Add(sessionView);
                    }
                }
                error.sessions = sessions;
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static SessionViewErrorCatcher GetSessionViews(int idCoach) {
            SessionViewErrorCatcher error = new SessionViewErrorCatcher();
            try
            {
                Coach coach = Core.Context.Coaches.Where(c => c.idCoach == idCoach).FirstOrDefault();
                error.sessions =
                    Core.Context.SessionViews
                    .ToList();
                List<SessionView> sessions = new List<SessionView>();
                foreach (SessionView session in error.sessions)
                {
                    if (coach.Sessions.Where(s => s.idSession == session.idSession).ToList().Count == 0)
                    {
                        sessions.Add(session);
                    }
                }
                error.sessions = sessions;
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static SessionViewErrorCatcher GetSessionViews()
        {
            SessionViewErrorCatcher error = new SessionViewErrorCatcher();
            try
            {
                error.sessions =
                    Core.Context.SessionViews
                    .ToList();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SaveSession(Session session)
        {
            ErrorCatcher errorCatcher = new ErrorCatcher();
            try
            {
                if (Core.Context.Sessions.Find(session.idSession) == null)
                {
                    Core.Context.Sessions.Add(session);
                }
                else
                {
                    Core.Context.Sessions.Attach(session);
                }
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                errorCatcher.setMessage(e);
            }

            return errorCatcher;
        }

        public static ErrorCatcher DeleteSession(Session session)
        {
            ErrorCatcher errorCatcher = new ErrorCatcher();
            try
            {
                Core.Context.Sessions.Remove(session);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                errorCatcher.setMessage(e);
            }

            return errorCatcher;
        }

        public static ErrorCatcher ArrangeCoach(Session session, Coach coach)
        {
            ErrorCatcher errorCatcher = new ErrorCatcher();
            try
            {
                session.Coaches.Add(coach);
                Core.Context.Sessions.Attach(session);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                errorCatcher.setMessage(e);
            }

            return errorCatcher;
        }

        public static ErrorCatcher RearrangeCoach(Session session, Coach coach)
        {
            ErrorCatcher errorCatcher = new ErrorCatcher();
            try
            {
                session.Coaches.Remove(coach);
                coach.Sessions.Remove(session);
                Core.Context.Sessions.Attach(session);
                Core.Context.Coaches.Attach(coach);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                errorCatcher.setMessage(e);
            }

            return errorCatcher;
        }
    }
}
