﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Drawing;
using Microsoft.Office.Interop.Word;

namespace FitnessClub.Models
{
    public static class SubscriptionTypesData
    {
        public static void PrintSubscriptionTypes()
        {

        }

        public static List<SubscriptionType> GetSubscriptionTypes()
        {
            return Core.Context.SubscriptionTypes.ToList();
        }

        public static List<PricedSubscriptionType> GetPricedSubscriptionTypes()
        {
            return Core.Context.PricedSubscriptionTypes.ToList();
        }

        public static SubscriptionTypeErrorCatcher GetSubcriptionTypeById(int idSubscriptionType)
        {
            SubscriptionTypeErrorCatcher error = new SubscriptionTypeErrorCatcher();
            try
            {
                error.subscriptionType = Core.Context.SubscriptionTypes.Where(st => st.idSubscriptionType == idSubscriptionType).FirstOrDefault();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SaveSubscriptionType(SubscriptionType subscriptionType)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                if (Core.Context.SubscriptionTypes.Find(subscriptionType.idSubscriptionType) == null)
                {
                    Core.Context.SubscriptionTypes.Add(subscriptionType);
                }
                else
                {
                    Core.Context.SubscriptionTypes.Attach(subscriptionType);
                }
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SavePricedSubscriptionType(PricedSubscriptionType subscriptionType)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                if (Core.Context.PricedSubscriptionTypes.Find(subscriptionType.idSubscriptionType) == null)
                {
                    Core.Context.PricedSubscriptionTypes.Add(subscriptionType);
                }
                else
                {
                    Core.Context.PricedSubscriptionTypes.Attach(subscriptionType);
                }
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DeleteSubscriptionType(SubscriptionType subscriptionType)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                Core.Context.SubscriptionTypes.Remove(subscriptionType); 
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }
    }
}
