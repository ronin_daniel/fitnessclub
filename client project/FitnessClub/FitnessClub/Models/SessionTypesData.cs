﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClub.Models
{
    public static class SessionTypesData
    {
        public static List<SessionType> GetSessionTypes()
        {
            return Core.Context.SessionTypes.ToList();
        }

        public static SessionTypesErrorCatcher GetCoachSessionTypes(int idCoach)
        {
            SessionTypesErrorCatcher error = new SessionTypesErrorCatcher();
            try
            {
                error.sessionTypes =
                    Core.Context.SessionTypes
                    .Where(st => st.Coaches.Where(c => c.idCoach == idCoach).ToList().Count() > 0)
                    .ToList();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static List<String> GetSessionTypesLabel()
        {
            return Core.Context.SessionTypes.Select(s => s.label).ToList();
        }

        public static IntErrorCatcher GetSessionTypeId(string label)
        {
            IntErrorCatcher error = new IntErrorCatcher();
            try
            {
                error.value = Core.Context.SessionTypes.Find(label).idSessionType;
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static SessionTypeErrorCatcher GetSessionType(int idSessionType)
        {
            SessionTypeErrorCatcher error = new SessionTypeErrorCatcher();
            try
            {
                error.sessionType = Core.Context.SessionTypes.Where(st => st.idSessionType == idSessionType).FirstOrDefault();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SaveSessionType(SessionType sessionType)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                if (Core.Context.SessionTypes.Find(sessionType.idSessionType) == null)
                {
                    Core.Context.SessionTypes.Add(sessionType);
                }
                else
                {
                    Core.Context.SessionTypes.Attach(sessionType);
                }
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DeleteSessionType(SessionType sessionType)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                Core.Context.SessionTypes.Remove(sessionType);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }
    }
}
