﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClub.Models
{
    public static class ClientsData
    {
        public static List<Client> GetClients()
        {
            return Core.Context.Clients.ToList();
        }

        public static ClientErrorCatcher GetClientByCurrentUser()
        {
            ClientErrorCatcher errorCatcher = new ClientErrorCatcher();
            try
            {
                errorCatcher.client = Core.Context.Clients.Where(c => c.name == CurrentUser.name && c.password == CurrentUser.password).FirstOrDefault();
            }
            catch (Exception e)
            {
                errorCatcher.setMessage(e);
            }

            return errorCatcher;
        }

        public static ErrorCatcher SaveClient(Client client)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                if (Core.Context.Clients.Find(client.idClient) == null)
                {
                    Core.Context.Clients.Add(client);
                }
                else
                {
                    Core.Context.Clients.Attach(client);
                }
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DeleteClient(Client client)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                Core.Context.Clients.Remove(client);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SaveSubscription(Subscription subscription, Client client)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                subscription.Client = client;
                Core.Context.Subscriptions.Add(subscription);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DeleteSubscription(int idSubscription)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                Subscription subscription = Core.Context.Subscriptions.Where(s => s.idSubscription == idSubscription).FirstOrDefault();
                Core.Context.Subscriptions.Remove(subscription);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SaveClientSession(Client client, Subscription subscription, Session session)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                client.Sessions.Add(session);
                subscription.Sessions.Add(session);
                Core.Context.Clients.Attach(client);
                Core.Context.Subscriptions.Attach(subscription);
                Core.Context.Sessions.Attach(session);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SaveSubscriptionSession(Subscription subscription, Session session)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                subscription.Sessions.Add(session);
                Core.Context.Subscriptions.Attach(subscription);
                Core.Context.Sessions.Attach(session);
                Core.Context.SaveChanges();
                //Subscription subscription = Core.Context.Subscriptions.Where(s => s.idSubscription == idSubscription).FirstOrDefault();
                //SubscriptionSession subscriptionSession = new SubscriptionSession();
                //Core.Context.SubscriptionSessions.Add(subscriptionSession);
                //session.SubscriptionSessions.Add(subscriptionSession);
                //subscription.SubscriptionSessions.Add(subscriptionSession);
                //Core.Context.Sessions.Attach(session);
                //Core.Context.Subscriptions.Attach(subscription);
                //Core.Context.SubscriptionSessions.Attach(subscriptionSession);
                //Core.Context.SaveChanges();
                //error.subscriptionSession = subscriptionSession;
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DeleteSubscriptionSession(Subscription subscription, Session session)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                subscription.Sessions.Remove(session);
                Core.Context.Subscriptions.Attach(subscription);
                Core.Context.Sessions.Attach(session);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static AmountSubscriptionErrorCatcher GetAmountSubscriptions(Client client)
        {
            AmountSubscriptionErrorCatcher error = new AmountSubscriptionErrorCatcher();
            try
            {
                error.amountSubscription = Core.Context.AmountSubscriptions.Where(s => s.idClient == client.idClient).ToList();
                foreach (AmountSubscription subscription in error.amountSubscription)
                {
                    Core.Context.Entry(subscription).Reload();
                }
                error.amountSubscription = error.amountSubscription.Where(s => s.Sessions_left > 0).ToList();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static SubscriptionErrorCatcher GetSubscription(int idSubscription)
        {
            SubscriptionErrorCatcher error = new SubscriptionErrorCatcher();
            try
            {
                error.subscription = Core.Context.Subscriptions.Where(s => s.idSubscription == idSubscription).FirstOrDefault();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DeleteClientSession(Client client, Session session)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                bool isSoloSession = session.SessionType.peopleAmount == 1;
                client.Sessions.Remove(session);
                foreach (Subscription sub in client.Subscriptions)
                {
                    if (sub.Sessions.Contains(session))
                    {
                        sub.Sessions.Remove(session);
                        Core.Context.Subscriptions.Attach(sub);
                        break;
                    }
                }

                if (isSoloSession)
                {
                    bool isWithCoach = session.Coaches.Count > 1;
                    if (isWithCoach)
                    {
                        List<Coach> coaches = session.Coaches.ToList();
                        foreach (Coach coach in coaches)
                        {
                            session.Coaches.Remove(coach);
                            Core.Context.Coaches.Attach(coach);
                        }
                    }
                    Core.Context.Sessions.Remove(session);
                }
                Core.Context.Clients.Attach(client);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DetachCoachFromSoloSession(Session session)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                List<Coach> coaches = session.Coaches.ToList();
                foreach (Coach coach in coaches)
                {
                    session.Coaches.Remove(coach);
                    Core.Context.Coaches.Attach(coach);
                }

                Core.Context.Sessions.Attach(session);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }
    }
}
