﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClub
{
    public static class CoachesData
    {
        public static List<Coach> GetCoaches()
        {
            return Core.Context.Coaches.ToList();
        }

        public static List<Coach> GetCoachesBySessionType(SessionType sessionType)
        {
            return
                Core.Context.Coaches
                .Where(c => c.SessionTypes.Where(st => st.idSessionType == sessionType.idSessionType).ToList().Count > 0)
                .ToList();
        }

        public static List<Coach> GetCoachesBySession(Session session)
        {
            return
                Core.Context.Coaches
                .Where(c => c.SessionTypes.Where(st => st.idSessionType == session.SessionType.idSessionType).ToList().Count > 0
                        && c.Sessions.Where(s => s.idSession == session.idSession).ToList().Count > 0)
                .ToList();
        }

        public static List<Coach> GetCoachesByNotInSession(Session session)
        {
            return
                Core.Context.Coaches
                .Where(c => c.SessionTypes.Where(st => st.idSessionType == session.SessionType.idSessionType).ToList().Count > 0
                        && c.Sessions.Where(s => s.idSession == session.idSession).ToList().Count == 0)
                .ToList();
        }

        public static CoachErrorCatcher GetCoachByCurrentUser()
        {
            CoachErrorCatcher error = new CoachErrorCatcher();
            try
            {
                error.coach = Core.Context.Coaches.Where(c => c.name == CurrentUser.name && c.password == CurrentUser.password).FirstOrDefault();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher SaveCoach(Coach coach)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                if (Core.Context.Coaches.Find(coach.idCoach) == null)
                {
                    Core.Context.Coaches.Add(coach);
                }
                else
                {
                    Core.Context.Coaches.Attach(coach);
                }
                Core.Context.SaveChanges();
            } 
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DeleteCoach(Coach coach)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                Core.Context.Coaches.Remove(coach);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher AttachSessionType(Coach coach, SessionType sessionType)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                coach.SessionTypes.Add(sessionType);
                Core.Context.Coaches.Attach(coach);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }

        public static ErrorCatcher DettachSessionType(Coach coach, SessionType sessionType)
        {
            ErrorCatcher error = new ErrorCatcher();
            try
            {
                coach.SessionTypes.Remove(sessionType);
                Core.Context.Coaches.Attach(coach);
                Core.Context.SaveChanges();
            }
            catch (Exception e)
            {
                error.setMessage(e);
            }

            return error;
        }
    }
}

