﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace FitnessClub
{
    public static class Core
    {
        private static FitnessClubEntities4 context;

        public static FitnessClubEntities4 Context
        {
            get
            {
                if (context == null)
                {
                    context = new FitnessClubEntities4();
                }

                return context;
            }
        }

        public static void RefreshContext()
        {
            context = new FitnessClubEntities4();
        }

        public static void ReconnectDatabase()
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.InitialCatalog = Properties.Settings.Default.Db;

                builder.DataSource = Properties.Settings.Default.server;

                builder.UserID = Properties.Settings.Default.login;

                builder.Password = Properties.Settings.Default.password;

                builder.IntegratedSecurity = true;

                Context.Database.Connection.ConnectionString = builder.ConnectionString;
            }
            catch
            {

            }
        }
    }
}
