USE [FitnessClub]
GO
/****** Object:  User [root]    Script Date: 5/12/2021 7:18:55 AM ******/
CREATE USER [root] FOR LOGIN [root] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [ADMINS]    Script Date: 5/12/2021 7:18:55 AM ******/
CREATE ROLE [ADMINS]
GO
/****** Object:  DatabaseRole [CLIENTS]    Script Date: 5/12/2021 7:18:55 AM ******/
CREATE ROLE [CLIENTS]
GO
/****** Object:  DatabaseRole [COACHES]    Script Date: 5/12/2021 7:18:55 AM ******/
CREATE ROLE [COACHES]
GO
ALTER ROLE [ADMINS] ADD MEMBER [root]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[idClient] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NOT NULL,
	[number] [varchar](11) NULL,
	[password] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[idClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_1]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_1]
AS
SELECT        idClient, name
FROM            dbo.Client
GO
/****** Object:  Table [dbo].[Coach]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coach](
	[idCoach] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NOT NULL,
	[password] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Coach] PRIMARY KEY CLUSTERED 
(
	[idCoach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[CoachData]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CoachData]
AS
SELECT        name, password
FROM            dbo.Coach
GO
/****** Object:  Table [dbo].[SubscriptionType]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriptionType](
	[idSubscriptionType] [int] IDENTITY(1,1) NOT NULL,
	[idSessionType] [int] NOT NULL,
	[sessionsAmount] [int] NOT NULL,
	[discountAsSubscription] [decimal](3, 2) NOT NULL,
 CONSTRAINT [PK_SubscriptionType] PRIMARY KEY CLUSTERED 
(
	[idSubscriptionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SessionType]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SessionType](
	[idSessionType] [int] IDENTITY(1,1) NOT NULL,
	[price] [decimal](10, 2) NOT NULL,
	[label] [varchar](20) NOT NULL,
	[peopleAmount] [tinyint] NOT NULL,
 CONSTRAINT [PK_SessionType] PRIMARY KEY CLUSTERED 
(
	[idSessionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[PricedSubscriptionType]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PricedSubscriptionType]
AS
	SELECT st.idSubscriptionType,
			st.idSessionType,
			st.sessionsAmount,
			st.discountAsSubscription,
			st.sessionsAmount * s.price -  (st.sessionsAmount * s.price * st.discountAsSubscription) as 'Price'
	FROM SubscriptionType as st
	JOIN SessionType as s ON s.idSessionType = st.idSessionType;
GO
/****** Object:  Table [dbo].[Subscription]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subscription](
	[idSubscription] [int] IDENTITY(1,1) NOT NULL,
	[idClient] [int] NOT NULL,
	[idSubscriptionType] [int] NOT NULL,
 CONSTRAINT [PK_Subscription] PRIMARY KEY CLUSTERED 
(
	[idSubscription] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscriptionSession]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriptionSession](
	[idSubscription] [int] NOT NULL,
	[idSession] [int] NOT NULL,
 CONSTRAINT [PK_SubscriptionSession] PRIMARY KEY CLUSTERED 
(
	[idSubscription] ASC,
	[idSession] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AmountSubscription]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AmountSubscription]
AS
SELECT        s.idSubscription, s.idClient, st.sessionsAmount -
                             (SELECT        COUNT(*) AS Expr1
                               FROM            dbo.SubscriptionSession AS ss
                               WHERE        (idSubscription = s.idSubscription)) AS [Sessions left], st.idSessionType, dbo.SessionType.label, st.sessionsAmount
FROM            dbo.Subscription AS s INNER JOIN
                         dbo.SubscriptionType AS st ON st.idSubscriptionType = s.idSubscriptionType INNER JOIN
                         dbo.SessionType ON st.idSessionType = dbo.SessionType.idSessionType
GO
/****** Object:  Table [dbo].[ClientSession]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientSession](
	[idClient] [int] NOT NULL,
	[idSession] [int] NOT NULL,
 CONSTRAINT [PK_ClientSession] PRIMARY KEY CLUSTERED 
(
	[idClient] ASC,
	[idSession] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Session]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Session](
	[idSession] [int] IDENTITY(1,1) NOT NULL,
	[idSessionType] [int] NOT NULL,
	[startTime] [datetime] NOT NULL,
	[endTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED 
(
	[idSession] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[SessionView]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SessionView]
AS
SELECT        dbo.Session.idSession, dbo.Session.startTime, dbo.Session.endTime, dbo.SessionType.label AS 'Session type', dbo.Client.idClient
FROM            dbo.Session INNER JOIN
                         dbo.SessionType ON dbo.Session.idSessionType = dbo.SessionType.idSessionType INNER JOIN
                         dbo.ClientSession ON dbo.Session.idSession = dbo.ClientSession.idSession INNER JOIN
                         dbo.Client ON dbo.ClientSession.idClient = dbo.Client.idClient
GO
/****** Object:  Table [dbo].[SessionWithCoach]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SessionWithCoach](
	[idSession] [int] NOT NULL,
	[idCoach] [int] NOT NULL,
 CONSTRAINT [PK_SessionWithCoach] PRIMARY KEY CLUSTERED 
(
	[idSession] ASC,
	[idCoach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[CoachSessionView]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CoachSessionView]
AS
SELECT        dbo.Session.idSession, dbo.Session.startTime, dbo.Session.endTime, dbo.SessionType.label AS 'Session type', dbo.Coach.idCoach
FROM            dbo.SessionType INNER JOIN
                         dbo.Session ON dbo.SessionType.idSessionType = dbo.Session.idSessionType INNER JOIN
                         dbo.SessionWithCoach ON dbo.Session.idSession = dbo.SessionWithCoach.idSession INNER JOIN
                         dbo.Coach ON dbo.SessionWithCoach.idCoach = dbo.Coach.idCoach
GO
/****** Object:  Table [dbo].[CoachSessionType]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoachSessionType](
	[idCoach] [int] NOT NULL,
	[idSessionType] [int] NOT NULL,
 CONSTRAINT [PK_CoachSessionType] PRIMARY KEY CLUSTERED 
(
	[idCoach] ASC,
	[idSessionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[password] [nvarchar](20) NOT NULL,
	[userRole] [nchar](10) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Session] ADD  CONSTRAINT [DF_Session_idSessionType]  DEFAULT ((1)) FOR [idSessionType]
GO
ALTER TABLE [dbo].[Subscription] ADD  CONSTRAINT [DF_Subscription_idSubscriptionType]  DEFAULT ((1)) FOR [idSubscriptionType]
GO
ALTER TABLE [dbo].[SubscriptionType] ADD  CONSTRAINT [DF_SubscriptionType_idSessionType]  DEFAULT ((1)) FOR [idSessionType]
GO
ALTER TABLE [dbo].[ClientSession]  WITH CHECK ADD  CONSTRAINT [client-session] FOREIGN KEY([idClient])
REFERENCES [dbo].[Client] ([idClient])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClientSession] CHECK CONSTRAINT [client-session]
GO
ALTER TABLE [dbo].[ClientSession]  WITH CHECK ADD  CONSTRAINT [session-client] FOREIGN KEY([idSession])
REFERENCES [dbo].[Session] ([idSession])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClientSession] CHECK CONSTRAINT [session-client]
GO
ALTER TABLE [dbo].[CoachSessionType]  WITH CHECK ADD  CONSTRAINT [coach-sessiontype] FOREIGN KEY([idCoach])
REFERENCES [dbo].[Coach] ([idCoach])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CoachSessionType] CHECK CONSTRAINT [coach-sessiontype]
GO
ALTER TABLE [dbo].[CoachSessionType]  WITH CHECK ADD  CONSTRAINT [sessiontype-coach] FOREIGN KEY([idSessionType])
REFERENCES [dbo].[SessionType] ([idSessionType])
GO
ALTER TABLE [dbo].[CoachSessionType] CHECK CONSTRAINT [sessiontype-coach]
GO
ALTER TABLE [dbo].[Session]  WITH CHECK ADD  CONSTRAINT [FK_Session_SessionType] FOREIGN KEY([idSessionType])
REFERENCES [dbo].[SessionType] ([idSessionType])
GO
ALTER TABLE [dbo].[Session] CHECK CONSTRAINT [FK_Session_SessionType]
GO
ALTER TABLE [dbo].[SessionWithCoach]  WITH CHECK ADD  CONSTRAINT [coach-session] FOREIGN KEY([idCoach])
REFERENCES [dbo].[Coach] ([idCoach])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SessionWithCoach] CHECK CONSTRAINT [coach-session]
GO
ALTER TABLE [dbo].[SessionWithCoach]  WITH CHECK ADD  CONSTRAINT [session-coach] FOREIGN KEY([idSession])
REFERENCES [dbo].[Session] ([idSession])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SessionWithCoach] CHECK CONSTRAINT [session-coach]
GO
ALTER TABLE [dbo].[Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_Client] FOREIGN KEY([idClient])
REFERENCES [dbo].[Client] ([idClient])
GO
ALTER TABLE [dbo].[Subscription] CHECK CONSTRAINT [FK_Subscription_Client]
GO
ALTER TABLE [dbo].[Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Subscription_SubscriptionType] FOREIGN KEY([idSubscriptionType])
REFERENCES [dbo].[SubscriptionType] ([idSubscriptionType])
ON DELETE SET DEFAULT
GO
ALTER TABLE [dbo].[Subscription] CHECK CONSTRAINT [FK_Subscription_SubscriptionType]
GO
ALTER TABLE [dbo].[SubscriptionSession]  WITH CHECK ADD  CONSTRAINT [session-subscription] FOREIGN KEY([idSubscription])
REFERENCES [dbo].[Session] ([idSession])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubscriptionSession] CHECK CONSTRAINT [session-subscription]
GO
ALTER TABLE [dbo].[SubscriptionSession]  WITH CHECK ADD  CONSTRAINT [subscription-session] FOREIGN KEY([idSubscription])
REFERENCES [dbo].[Subscription] ([idSubscription])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubscriptionSession] CHECK CONSTRAINT [subscription-session]
GO
ALTER TABLE [dbo].[SubscriptionType]  WITH CHECK ADD  CONSTRAINT [FK_SubscriptionType_SessionType] FOREIGN KEY([idSessionType])
REFERENCES [dbo].[SessionType] ([idSessionType])
ON DELETE SET DEFAULT
GO
ALTER TABLE [dbo].[SubscriptionType] CHECK CONSTRAINT [FK_SubscriptionType_SessionType]
GO
ALTER TABLE [dbo].[SessionType]  WITH CHECK ADD  CONSTRAINT [CK_SessionType] CHECK  (([peopleAmount]<=(20) AND [peopleAmount]>(0)))
GO
ALTER TABLE [dbo].[SessionType] CHECK CONSTRAINT [CK_SessionType]
GO
ALTER TABLE [dbo].[SubscriptionType]  WITH CHECK ADD  CONSTRAINT [discount] CHECK  (([discountAsSubscription]>(0) AND [discountAsSubscription]<(1)))
GO
ALTER TABLE [dbo].[SubscriptionType] CHECK CONSTRAINT [discount]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [role] CHECK  (([userRole]='client' OR [userRole]='coach' OR [userRole]='admin'))
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [role]
GO
/****** Object:  StoredProcedure [dbo].[CreateClient]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateClient]
	@login nvarchar(20),
	@password nvarchar(20)
	AS
	BEGIN
		DECLARE @sql nvarchar(max);
		set @sql = 'use FitnessClub;' +
					'create login ' + @login +
					'with password = ''' + @password + '''; ' +
					'create user ' + @login + ' from login '
					+ @login + ';' +
	'exec sp_addrolemember ''CLIENTS'', ' + '''' + @login + ''';';
		exec (@sql);
	END
GO
/****** Object:  StoredProcedure [dbo].[CreateCoach]    Script Date: 5/12/2021 7:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateCoach]
	@name nvarchar(20),
	@password nvarchar(20)
	AS
	BEGIN
		DECLARE @sql nvarchar(max);
		set @sql = 'use FitnessClub;' +
					'create login ' + @name +
					'with password = ''' + @password + '''; ' +
					'create user ' + @name + ' from login '
					+ @name + ';' +
	'exec sp_addrolemember ''CLIENTS'', ' + '''' + @name + ''';';
		exec (@sql);
	END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "st"
            Begin Extent = 
               Top = 92
               Left = 346
               Bottom = 222
               Right = 560
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SessionType"
            Begin Extent = 
               Top = 6
               Left = 598
               Bottom = 136
               Right = 768
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AmountSubscription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AmountSubscription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Coach"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CoachData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CoachData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Coach"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Session"
            Begin Extent = 
               Top = 141
               Left = 222
               Bottom = 271
               Right = 392
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SessionType"
            Begin Extent = 
               Top = 100
               Left = 537
               Bottom = 230
               Right = 707
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SessionWithCoach"
            Begin Extent = 
               Top = 5
               Left = 381
               Bottom = 101
               Right = 551
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CoachSessionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CoachSessionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Session"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SessionType"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Client"
            Begin Extent = 
               Top = 55
               Left = 897
               Bottom = 185
               Right = 1067
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ClientSession"
            Begin Extent = 
               Top = 6
               Left = 662
               Bottom = 102
               Right = 832
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SessionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SessionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Client"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
