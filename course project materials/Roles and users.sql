USE FitnessClub
GO
	CREATE ROLE CLIENTS AUTHORIZATION [dbo];
	GRANT INSERT,UPDATE,SELECT ON dbo.ClientSession TO CLIENTS;
	GRANT INSERT,UPDATE,SELECT ON dbo.Subscription TO CLIENTS;
	GRANT INSERT,UPDATE,SELECT ON dbo.SubscriptionSession TO CLIENTS;
	GRANT INSERT,UPDATE,SELECT ON dbo.Session TO CLIENTS;

	CREATE ROLE COACHES AUTHORIZATION [dbo];
	GRANT INSERT,UPDATE,SELECT ON dbo.Session TO COACHES;
	GRANT INSERT,UPDATE,SELECT ON dbo.SessionWithCoach TO COACHES;
	GRANT INSERT,UPDATE,SELECT ON dbo.CoachSessionType TO COACHES;

	CREATE ROLE ADMINS AUTHORIZATION [dbo];
	GRANT INSERT,UPDATE,SELECT ON dbo.Client TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.ClientSession TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.Coach TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.CoachSessionType TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.Session TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.SessionType TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.SessionWithCoach TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.Subscription TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.SubscriptionSession TO ADMINS;
	GRANT INSERT,UPDATE,SELECT ON dbo.SubscriptionType TO ADMINS;

	CREATE LOGIN root
	WITH PASSWORD = 'root',
	DEFAULT_DATABASE = FitnessClub, 
	CHECK_EXPIRATION=OFF,
    CHECK_POLICY=ON;

	CREATE USER root
	FOR LOGIN root;

	EXEC sp_addrolemember 'ADMINS', 'root'
GO

USE FitnessClub
GO
ALTER PROCEDURE CreateClient
	@login nvarchar(20),
	@password nvarchar(20)
	AS
	BEGIN
		DECLARE @sql nvarchar(max);
		set @sql = 'use FitnessClub;' +
					'create login ' + @login +
					'with password = ''' + @password + '''; ' +
					'create user ' + @login + ' from login '
					+ @login + ';' +
	'exec sp_addrolemember ''CLIENTS'', ' + '''' + @login + ''';';
		exec (@sql);
	END
GO

USE FitnessClub
GO
CREATE PROCEDURE CreateCoach
	@name nvarchar(20),
	@password nvarchar(20)
	AS
	BEGIN
		DECLARE @sql nvarchar(max);
		set @sql = 'use FitnessClub;' +
					'create login ' + @name +
					'with password = ''' + @password + '''; ' +
					'create user ' + @name + ' from login '
					+ @name + ';' +
	'exec sp_addrolemember ''CLIENTS'', ' + '''' + @name + ''';';
		exec (@sql);
	END
GO
