USE [FitnessClub]
GO
CREATE PROCEDURE GetUsers
AS
BEGIN
SET NOCOUNT ON
 
SELECT * FROM [User];
 
END
GO

USE [FitnessClub]
GO
CREATE PROCEDURE GetCoaches
AS
BEGIN
SET NOCOUNT ON
 
SELECT * FROM Coach;
 
END
GO

USE [FitnessClub]
GO
CREATE FUNCTION GetUserName (@idUser int)
RETURNS int AS
BEGIN
	DECLARE @return_value nvarchar(20);
	SELECT @return_value = idUser FROM [User] 
	WHERE [User].name = @idUser;
    
	RETURN @return_value;
END;
GO

USE [FitnessClub]
GO
CREATE FUNCTION GetUserId (@name nvarchar(20))
RETURNS int AS
BEGIN
	DECLARE @return_value int;
	SELECT @return_value = idUser FROM [User] 
	WHERE [User].name = @name;
    
	RETURN @return_value;
END;
GO