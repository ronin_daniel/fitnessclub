ALTER VIEW PricedSubscriptionType
AS
	SELECT st.idSubscriptionType,
			st.idSessionType,
			st.sessionsAmount,
			st.discountAsSubscription,
			st.sessionsAmount * s.price -  (st.sessionsAmount * s.price * st.discountAsSubscription) as 'Price'
	FROM SubscriptionType as st
	JOIN SessionType as s ON s.idSessionType = st.idSessionType;

CREATE VIEW AmountSubscription
AS
	SELECT s.idSubscription,
			s.idClient, 
			st.sessionsAmount - (SELECT COUNT(*) FROM SubscriptionSession as ss WHERE ss.idSubscription = s.idSubscription) as 'Sessions left'
	FROM Subscription as s
	JOIN SubscriptionType as st ON st.idSubscriptionType = s.idSubscriptionType;