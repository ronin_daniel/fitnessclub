--DELETE SUBSCRIPTION TRIGGER
USE FitnessClub
GO
CREATE TRIGGER DeleteSessionTypeTrigger
ON SessionType
INSTEAD OF DELETE
AS 
BEGIN
	IF (SELECT idSessionType FROM deleted) NOT IN (SELECT idSessionType FROM CoachSessionType) 
	   AND (SELECT idSessionType FROM deleted) NOT IN (SELECT idSessionType FROM Session) 
	   AND (SELECT idSessionType FROM deleted) NOT IN (SELECT idSessionType FROM SubscriptionType) 
		DELETE FROM SessionType
		WHERE SessionType.idSessionType IN (SELECT idSessionType FROM deleted);
	ELSE
		RAISERROR('Session type is occupied',16,1);
END
GO

USE FitnessClub
GO
CREATE TRIGGER DeleteSubscriptionTypeTrigger
ON SubscriptionType
INSTEAD OF DELETE
AS 
BEGIN
	IF (SELECT idSubscriptionType FROM deleted) NOT IN (SELECT idSubscriptionType FROM Subscription) 
		DELETE FROM SubscriptionType
		WHERE SubscriptionType.idSubscriptionType IN (SELECT idSubscriptionType FROM deleted);
	ELSE
		RAISERROR('Subscription type is occupied',16,1);
END
GO

USE FitnessClub
GO
CREATE TRIGGER AddCoachSessionTrigger
ON SessionWithCoach
INSTEAD OF INSERT
AS 
BEGIN
		DECLARE @count int;
		SELECT @count = COUNT(*) FROM SessionWithCoach;
		INSERT INTO SessionWithCoach
	(idSession, idCoach)
	SELECT inserted.idSession, inserted.idCoach
	FROM inserted
	JOIN Session as s ON s.idSession = inserted.idSession
	JOIN CoachSessionType as cst ON cst.idCOach = inserted.idCoach
	WHERE NOT EXISTS
		(
		SELECT * FROM Session as qs
		JOIN SessionWithCoach as cs on qs.idSession = cs.idSession
		WHERE cs.idCoach = inserted.idCoach AND
			(s.startTime <= qs.endTime AND s.endTime >= qs.startTime)
		) 
		AND cst.idSessionType = s.idSessionType;
	IF @count = (SELECT COUNT(*) FROM SessionWithCoach)
		RAISERROR('Coach schedules is occupied or wrong session type',16,1);
END
GO

USE FitnessClub
GO
CREATE TRIGGER AddSubscriptionSessionTrigger
ON SubscriptionSession
INSTEAD OF INSERT
AS 
BEGIN
	DECLARE @count int;
	SELECT @count = COUNT(*) FROM SubscriptionSession;
	INSERT INTO SubscriptionSession
	(idSubscription, idSession)
	SELECT inserted.idSubscription, inserted.idSession
	FROM inserted
	JOIN Subscription as s ON s.idSubscription = inserted.idSubscription
	JOIN SubscriptionType as st ON st.idSubscriptionType = s.idSubscriptionType
	WHERE (SELECT COUNT(*) FROM SubscriptionSession as ss
			WHERE ss.idSubscription = inserted.idSubscription) < st.sessionsAmount;
	IF @count = (SELECT COUNT(*) FROM SubscriptionSession)
		RAISERROR('You have reached your sessions limit',16,1);
END
GO


USE FitnessClub
GO
CREATE TRIGGER AddClientSession
ON ClientSession
INSTEAD OF INSERT
AS 
BEGIN
	DECLARE @count int;
	SELECT @count = COUNT(*) FROM ClientSession;
	INSERT INTO ClientSession 
	(idClient, idSession)
	SELECT inserted.idClient, inserted.idSession
	FROM inserted
	JOIN Session as s ON s.idSession = inserted.idSession
	JOIN SessionType as st ON st.idSessionType = s.idSessionType
	WHERE (SELECT COUNT(*) FROM ClientSession WHERE idSession = s.idSession) < st.peopleAmount
	AND inserted.idClient NOT IN (SELECT idClient FROM ClientSession WHERE idSession = s.idSession)
	AND  NOT EXISTS
		(
		SELECT * FROM Session as qs
		WHERE inserted.idClient IN (SELECT idClient FROM ClientSession WHERE idSession = qs.idSession)
		AND
			(s.startTime <= qs.endTime AND s.endTime >= qs.startTime)
		);
	IF @count = (SELECT COUNT(*) FROM ClientSession)
		RAISERROR('Session is full, you are already in, or your schedule is packed!',16,1);
END
GO

USE FitnessClub
GO
CREATE TRIGGER AddClient
ON Client
AFTER INSERT
AS 
BEGIN
	INSERT INTO [User]
	(name, password, userRole)
	SELECT inserted.name, inserted.password, 'client'
	FROM inserted;
END
GO

USE FitnessClub
GO
CREATE TRIGGER AddCoach
ON Coach
AFTER INSERT
AS 
BEGIN
	INSERT INTO [User]
	(name, password, userRole)
	SELECT inserted.name, inserted.password, 'coach'
	FROM inserted;
END
GO

USE FitnessClub
GO
CREATE TRIGGER DeleteCoachSessionType
ON CoachSessionType
AFTER DELETE
AS 
BEGIN
	DELETE cs FROM SessionWithCoach as cs
	JOIN Session as s ON s.idSession = cs.idSession
	JOIN SessionType as st ON s.idSessionType = st.idSessionType
	WHERE cs.idCoach IN (SELECT idCoach FROM deleted)
			AND st.idSessionType IN (SELECT idSessionType FROM deleted);
END
GO

USE FitnessClub
GO
CREATE TRIGGER AddClientSessionTrigger
ON ClientSession
INSTEAD OF INSERT
AS 
BEGIN
		DECLARE @count int;
		SELECT @count = COUNT(*) FROM ClientSession;
		INSERT INTO ClientSession
	(idSession, idCLient)
	SELECT inserted.idSession, inserted.idClient
	FROM inserted
	JOIN Session as s ON s.idSession = inserted.idSession
	WHERE NOT EXISTS
		(
		SELECT * FROM Session as qs
		WHERE inserted.idClient IN (SELECT idClient FROM ClientSession WHERE idSession = qs.idSession)
		AND
			(s.startTime <= qs.endTime AND s.endTime >= qs.startTime)
		);
	IF @count = (SELECT COUNT(*) FROM ClientSession)
		RAISERROR('You have already got a session at that time',16,1);
END
GO